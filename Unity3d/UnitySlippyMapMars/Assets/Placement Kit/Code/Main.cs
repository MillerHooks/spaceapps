﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class Main : MonoBehaviour 
{
    public static string LoadResourceTextfile(string path)
    {

        string filePath = path.Replace(".json", "");

        TextAsset targetFile = Resources.Load<TextAsset>(filePath);

        return targetFile.text;
    }

	// Use this for initialization
	void Start () 
    {
        string testJSONData = LoadResourceTextfile("testData.json");
            
        JSONNode node = JSONNode.Parse( testJSONData ); //JSONNode.Parse("{\"name\":\"test\", \"array\":[1,{\"data\":\"value\"}]}");
        //WorldHistory miller = new WorldHistory(node);

        Debug.Log( node );
	}
}
