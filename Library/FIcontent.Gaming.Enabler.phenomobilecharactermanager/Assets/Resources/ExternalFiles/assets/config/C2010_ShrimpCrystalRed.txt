{
"ID Game element : C 2010 | Shrimp Crystal Red":
    [
    	{                
            "id": "TEST",
            "value": "TEST",
            "enum" : "Test",
            "unit" : "TEST",
            "type" : "TEST",
            "Comment": "TEST"
        },
		{                    
            "id" : "sex",
            "enum" : "male",
            "enum" : "female",
            "Comment": "sex of the shrimp"
        },
        {                  
            "id": "pregnant",
            "type" : "BOOL",
            "Comment": "variable is set to TRUE when fertilization has taken place AND sex is female"
        },
		{                 
            "id": "pregnancyCurrent",
            "value": "0",
            "unit" : "days",
            "type" : "FLOAT",
            "Comment": "current duration of pregnancy, beginning from "
        },
        {                
            "id": "pregnancyDuration",
            "value": "0",
            "unit" : "days",
            "type" : "FLOAT",
            "Comment": "duration of complete pregnancy beginning from fertilization until oviposition"
        },
        {                   
            "id": "health",
            "enum" : "normal",
            "enum" : "sick",
            "enum" : "AAAVitality",
            "enum" : "dead",
            "Comment": "normal health status, normal movements, normally coloured texture, sick health status, less movements, change of texture very vital health status, capable of reproduction because of ideal condition of organism, badge shown over organism indicates state dead, no movements, lying aside, dead texture "
        },
        {                
            "id": "ageValue",
            "enum" : "biological",
            "enum" : "current",
            "enum" : "maturity",
            "enum" : "lifespan",
            "Comment": " biological age of organism in days, influcenced by environment and other species, days are added when stress, hunger or overfeeding, etc. occurs. With this variable we can shorten the life of an organism caused by bad environmental conditions, current age of organism in days, beginning from birth, age in days when organism typically becomes adult and reaches sexual maturity, The typical duration of life of the organism "
        },
        {                
            "id": "ageStatus",
            "enum" : "child",
            "enum" : "adult",
            "Comment": "flag on/off after certain value for age_current is reached"
        },
        {                
            "id": "sizeValue",
            "value": "0",
            "unit" : "cm",
            "type" : "FLOAT",
            "Comment": "size of organism"
        },
        {                  
            "id": "sizeBirth",
            "value": "0",
            "unit" : "cm",
            "type" : "FLOAT",
            "Comment": "size of organism at birth (day 1)"
        },
        {                 
            "id": "currentSize",
            "value": "0",
            "unit" : "%",
            "type" : "FLOAT",
            "Comment": "current_size is calculated as percentage of adult size"
        },
        {                 
            "id": "inFoodCarbohydrate",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current amount of Carbohydrate"
        },
        {                   
            "id": "inFoodFat",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current amount of Fat"
        },
        {                  
            "id": "inFoodProtein",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current amount of Protein"
        },
        {                 
            "id": "inTemperature",
            "value": "0",
            "unit" : "°C",
            "type" : "FLOAT",
            "Comment": "Current Temperature"
        },
        {                   
            "id": "inPlantsCarbohydrate",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current amount of PlantsCarbohydrate"
        },
        {                  
            "id": "inPlantsFat",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current amount of PlantsFat"
        },
        {                   
            "id": "inPlantProtein",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current amount of PlantProtein"
        },
        {                 
            "id": "inOxygene",
            "value": "0",
            "unit" : "mg O2/l",
            "type" : "FLOAT",
            "Comment": "Current amount of Oxygene"
        },
        {                   
            "id": "outShrimpsCarbohydrate",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current outgoing amount of ShrimpsCabohydrate"
        },
        {                 
            "id": "outShrimpsFat",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current outgoing amount of ShrimpsFat"
        },
        {                    
            "id": "outShrimpsProtein",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current outgoing amount of ShrimpsProtein"
        },
        {                
            "id": "outAmmonium",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current outgoing amount of Ammonium"
        },
        {                   
            "id": "outCarbondioxid",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current outgoing amount of Carbondioxid"
        },
        {                   
            "id": "outPhosphat",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Current outgoing amount of Phosphat"
        },
        {                   
            "id": "optimalTemperature",
            "value": "0",
            "unit" : "°C",
            "type" : "FLOAT",
            "Comment": "Current outgoing Temperature"
        },
        {                  
            "id": "deadlyTemperatureLow",
            "value": "0",
            "unit" : "°C",
            "type" : "FLOAT",
            "Comment": "zeitliche Spanne, die überschritten werden muss, um tödlich zu sein?"
        },
        {                   
            "id": "deadlyTemperatureHigh",
            "value": "0",
            "unit" : "°C",
            "type" : "FLOAT",
            "Comment": "zeitliche Spanne, die überschritten werden muss, um tödlich zu sein?"
        },
        {               
            "id": "optimalOxygene",
            "value": "0",
            "unit" : "mg O2/l",
            "type" : "FLOAT",
            "Comment": "Optimal Oxygene level"
        },
        {                   
            "id": "deadlyOxygeneLow",
            "value": "0",
            "unit" : "mg O2/l",
            "type" : "FLOAT",
            "Comment": "zeitliche Spanne, die überschritten werden muss, um tödlich zu sein?"
        },
        {                  
            "id": "deadlyOxygeneHigh",
            "value": "0",
            "unit" : "mg O2/l",
            "type" : "FLOAT",
            "Comment": "zeitliche Spanne, die überschritten werden muss, um tödlich zu sein?"
        },
        {                  
            "id": "optimalPHvalue",
            "value": "0",
            "unit" : "pH",
            "type" : "FLOAT",
            "Comment": "Optimal PHValue"
        },
        {                   
            "id": "deadlyPHvalueLow",
            "value": "0",
            "unit" : "pH",
            "type" : "FLOAT",
            "Comment": "zu niedriger PH wert tödlich oder gesundheitsschädlich? zeitliche Spanne, die überschritten werden muss, um tödlich zu sein?"
        },
        {                
            "id": "deadlyPHvalueHigh",
            "value": "0",
            "unit" : "pH",
            "type" : "FLOAT",
            "Comment": "zu niedriger PH wert tödlich oder gesundheitsschädlich? zeitliche Spanne, die überschritten werden muss, um tödlich zu sein?"
        },
        {                
            "id": "optimalCarbohydrate",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Optimal Carbonhydrate Level"
        },
        {                   
            "id": "deadlyCarbohydrateLow",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "verhungern durch zuwenig Kohlenhydrate? Welche Einheit, zeitliche Spanne, die überschritten werden muss, welcher Wertebereich?"
        },
        {                   
            "id": "deadlyCarbohydrateHigh",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Tod durch Überfressen? Welche Einheit,  zeitliche Spanne, die überschritten werden muss, welcher Wertebereich?"
        },
        {                   
            "id": "optimalFat",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Optimal Fat Level"
        },
        {                  
            "id": "deadlyFatLow",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "verhungern durch zuwenig Fette? Welche Einheit, zeitliche Spanne, die überschritten werden muss, welcher Wertebereich?"
        },
        {                 
            "id": "deadlyFatHigh",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Tod durch Überfressen? Welche Einheit, zeitliche Spanne, die überschritten werden muss, welcher Wertebereich?"
        },
        {                
            "id": "optimalProtein",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Optimal Protein Level"
        },
        {                   
            "id": "deadlyProteinLow",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "verhungern durch zuwenig Protein? Welche Einheit, Zeit spielt bestimmt eine Rolle, welcher Wertebereich?"
        },
        {                 
            "id": "deadlyProteinHigh",
            "value": "0",
            "unit" : "mg/l",
            "type" : "FLOAT",
            "Comment": "Tod durch Überfressen? Welche Einheit, Zeit spielt bestimmt eine Rolle, welcher Wertebereich?"
        }
     ]
}