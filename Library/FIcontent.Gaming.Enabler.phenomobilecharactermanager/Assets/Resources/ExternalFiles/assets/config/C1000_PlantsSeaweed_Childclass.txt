{
    "ID Game element : C 0000 | Game Objects (GETY) PlantsSeaweed":
    [
    	{                
            "id": "TEST",
            "GETY": "TEST",
        },
        {                
            "id": "C0000",
            "GETY": "Algen",
        },
        {                
            "id": "C1010",
            "GETY": "Anubias Barteri",
        },
        {                
            "id": "C1020",
            "GETY": "Cryptocoryne Wendtii",
        },
        {                
            "id": "C1030",
            "GETY": "Eleocharis Pusilla",
        },
        {                
            "id": "C1040",
            "GETY": "Lobelia Cardinalis",
        },
        {                
            "id": "C1050",
            "GETY": "Ludwigia Repens",
        },
        {                
            "id": "C1060",
            "GETY": "Pogostemon Helferi",
        },
        {                
            "id": "C1070",
            "GETY": "Rotala Rotundifolia",
        },
     ]
}
