﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Epigene.GAME;

public class TestAnimation : MonoBehaviour 
{
	public List<Sprite> sprite = new List<Sprite>();
	public SpriteRenderer renderer;
	int actualSprite;
	public DateTime finishDate;
	GameManager gm;
	
	// Use this for initialization
	void Start () 
	{
		//renderer = GetComponent<SpriteRenderer>();
		gm = GameManager.Instance;

		Sprite[] sprites = Resources.LoadAll <Sprite> ("Test/Anubias_sprite"); 
		foreach (Sprite a in sprites)
		{
			sprite.Add(a);
		}

		
		StartCoroutine("animation");
	}
	
	private IEnumerator animation()
	{
		while (gameObject.activeSelf)
		{
			actualSprite++;
			if (actualSprite >= sprite.Count) actualSprite = 0;
			renderer.sprite = sprite[actualSprite];
			if (gm.Get("SIM").Time() > finishDate) Destroy(this.gameObject);
			yield return new WaitForSeconds(0.02f);
		}
	}
}
