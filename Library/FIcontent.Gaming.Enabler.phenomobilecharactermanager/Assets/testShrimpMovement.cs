﻿using UnityEngine;
using System.Collections;

public class testShrimpMovement : MonoBehaviour {
	
	public float xMax 	= 1.0f;
	public float xMin 	= -1.0f;
	public float yMax 	= 1.0f;
	public float yMin 	= -1.0f;
	public float zMax 	= 1.0f;
	public float zMin 	= -1.0f;

	public float movementSpeed 	= 5.0f;
	public float rotationSpeed 	= 0.5f;

	private Vector3 direction;

	bool exist;

	// Use this for initialization
	void Start () {
		exist = true;
		StartCoroutine("ChangeDirection");
		direction = new Vector3 (
			Random.Range(-180.0f, 180.0f),
			Random.Range(-180.0f, 180.0f),
			Random.Range(-180.0f, 180.0f));
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * Time.deltaTime * movementSpeed;

		transform.Rotate(
			direction
			* Time.deltaTime
			* rotationSpeed);

		if (transform.localPosition.x > xMax) transform.localPosition = new Vector3(xMax, transform.localPosition.y, transform.localPosition.z);
		if (transform.localPosition.x < xMin) transform.localPosition = new Vector3(xMin, transform.localPosition.y, transform.localPosition.z);
		if (transform.localPosition.y > yMax) transform.localPosition = new Vector3(transform.localPosition.x, yMax, transform.localPosition.z);
		if (transform.localPosition.y < yMin) transform.localPosition = new Vector3(transform.localPosition.x, yMin, transform.localPosition.z);
		if (transform.localPosition.z > zMax) transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, zMax);
		if (transform.localPosition.z < zMin) transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, zMin);

	}

	IEnumerator ChangeDirection()
	{
		while (exist)
		{
			/*transform.Rotate(new Vector3 (
				Random.Range(-3.0f, 3.0f),
				Random.Range(-3.0f, 3.0f),
				Random.Range(-3.0f, 3.0f)));

			 transform.rotation =
			Quaternion.Euler (
				transform.rotation.x + Random.Range(-1.0f, 1.0f),
				transform.rotation.y + Random.Range(-1.0f, 1.0f),
				transform.rotation.z + Random.Range(-1.0f, 1.0f));*/

			direction = new Vector3 (
				Random.Range(-180.0f, 180.0f),
				Random.Range(-180.0f, 180.0f),
				Random.Range(-180.0f, 180.0f));

			yield return new WaitForSeconds(Random.Range(0.5f, 3.0f));
		}
	}
}
