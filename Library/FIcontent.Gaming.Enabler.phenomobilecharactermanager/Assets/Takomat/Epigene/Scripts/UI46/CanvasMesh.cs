using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

using Epigene.UI;

public class CanvasMesh : Graphic
{
	/// <summary>
	/// Get/Set layer order
	/// </summary>
	public int SortingOrder
	{
		get { return sortingOrder; }
		set 
		{
			sortingOrder = value;
			if (order != null)
			{
				order.SetSoringOrder(this, sortingOrder);
			}
		}
	}
	private int sortingOrder = 0;
	public SortingOrder46 order = null;

	private List<UIVertex> vertices = new List<UIVertex>();

	public void SetMesh(List<Vector3> _vertices, List<Color32> colors)
	{
		vertices.Clear();
		
		UIVertex vert = UIVertex.simpleVert;

		float w = rectTransform.rect.width;
		float h = rectTransform.rect.height;

		for (int i = 0; i < _vertices.Count; i += 4)
		{
			vert.color = colors[i];
			vert.position = new Vector2(_vertices[i].x * w, _vertices[i].y * h);
			vertices.Add(vert);

			vert.color = colors[i + 1];
			vert.position = new Vector2(_vertices[i + 1].x * w, _vertices[i + 1].y * h);
			vertices.Add(vert);

			vert.color = colors[i + 2];
			vert.position = new Vector2(_vertices[i + 2].x * w, _vertices[i + 2].y * h);
			vertices.Add(vert);

			vert.color = colors[i + 3];
			vert.position = new Vector2(_vertices[i + 3].x * w, _vertices[i + 3].y * h);
			vertices.Add(vert);
		}
		SetVerticesDirty();
	}

	public void SetMesh(List<Vector3> _vertices, List<int> triangles, Color32 color)
	{
		vertices.Clear();

		UIVertex vert = UIVertex.simpleVert;
		vert.color = color;
	
		float w = rectTransform.rect.width;
		float h = rectTransform.rect.height;

		for (int i = 0; i < _vertices.Count; i += 4)
		{
			vert.position = new Vector2(_vertices[i].x * w, _vertices[i].y * h);
			vertices.Add(vert);

			vert.position = new Vector2(_vertices[i + 1].x * w, _vertices[i + 1].y * h);
			vertices.Add(vert);

			vert.position = new Vector2(_vertices[i + 2].x * w, _vertices[i + 2].y * h);
			vertices.Add(vert);

			vert.position = new Vector2(_vertices[i + 3].x * w, _vertices[i + 3].y * h);
			vertices.Add(vert);
		}
		SetVerticesDirty();
	}


	protected override void OnFillVBO(List<UIVertex> vbo)
	{
		vbo.Clear();
		if (vertices.Count == 0)
		{
			MeshFilter mf = gameObject.GetComponent<MeshFilter>();
			if (mf != null)
			{
				Mesh mesh = mf.sharedMesh;
				Vector3[] v = mesh.vertices;
				int[] t = mesh.triangles;
				Color[] c = mesh.colors;

				float ppu = 100;
				if (canvas != null)
				{
					ppu = canvas.referencePixelsPerUnit;
				}

				UIVertex vert = UIVertex.simpleVert;
				for (int i = 0; i < t.Length; i+=3)
				{
					vert.position = new Vector2(v[t[i]].x * ppu, v[t[i]].y * ppu);
					vert.color = color;
					vertices.Add(vert);
					vertices.Add(vert);

					vert.position = new Vector2(v[t[i + 1]].x * ppu, v[t[i + 1]].y * ppu);
					vert.color = color;
					vertices.Add(vert);

					vert.position = new Vector2(v[t[i + 2]].x * ppu, v[t[i + 2]].y * ppu);
					vert.color = color;
					vertices.Add(vert);
				}
			}
		}	
		vbo.AddRange(vertices);
	}
}