// Shader created with Shader Forge v1.06 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.06;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:True,dith:0,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.6103804,fgcg:0.1211073,fgcb:0.8235294,fgca:0.6588235,fgde:0,fgrn:0,fgrf:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:2366,x:32945,y:32703,varname:node_2366,prsc:2|custl-6544-OUT;n:type:ShaderForge.SFN_Multiply,id:1897,x:32459,y:32777,varname:node_1897,prsc:2|A-8987-RGB,B-9759-RGB,C-8288-OUT;n:type:ShaderForge.SFN_Tex2d,id:8987,x:32233,y:32760,varname:node_8987,prsc:2,tex:c95488444b35e4240a5eec64af9f0118,ntxv:0,isnm:False|UVIN-2270-OUT,TEX-3209-TEX;n:type:ShaderForge.SFN_Append,id:2270,x:32041,y:32760,varname:node_2270,prsc:2|A-8392-OUT,B-9952-OUT;n:type:ShaderForge.SFN_LightColor,id:9759,x:32233,y:32924,varname:node_9759,prsc:2;n:type:ShaderForge.SFN_LightAttenuation,id:8288,x:32233,y:33065,varname:node_8288,prsc:2;n:type:ShaderForge.SFN_Tex2dAsset,id:3209,x:32041,y:32941,ptovrint:False,ptlb:RAMP,ptin:_RAMP,varname:node_3209,tex:c95488444b35e4240a5eec64af9f0118,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:8392,x:31663,y:32757,varname:node_8392,prsc:2|A-8938-OUT,B-2870-OUT;n:type:ShaderForge.SFN_Dot,id:8938,x:31462,y:32757,varname:node_8938,prsc:2,dt:0|A-3044-OUT,B-7446-OUT;n:type:ShaderForge.SFN_ViewVector,id:7446,x:31237,y:32757,varname:node_7446,prsc:2;n:type:ShaderForge.SFN_LightVector,id:3223,x:31237,y:32598,varname:node_3223,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:3044,x:31237,y:32435,prsc:2,pt:False;n:type:ShaderForge.SFN_Dot,id:2720,x:31462,y:32520,varname:node_2720,prsc:2,dt:0|A-3044-OUT,B-3223-OUT;n:type:ShaderForge.SFN_Multiply,id:7250,x:31663,y:32520,varname:node_7250,prsc:2|A-2720-OUT,B-5004-OUT;n:type:ShaderForge.SFN_Add,id:9952,x:31859,y:32520,varname:node_9952,prsc:2|A-7250-OUT,B-8954-OUT;n:type:ShaderForge.SFN_Vector1,id:2870,x:31663,y:32889,varname:node_2870,prsc:2,v1:0.8;n:type:ShaderForge.SFN_Vector1,id:5004,x:31663,y:32650,varname:node_5004,prsc:2,v1:0.3;n:type:ShaderForge.SFN_Vector1,id:8954,x:31859,y:32650,varname:node_8954,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Color,id:4820,x:32233,y:33244,ptovrint:False,ptlb:node_4820,ptin:_node_4820,varname:node_4820,prsc:2,glob:False,c1:0.04411763,c2:1,c3:0.7626776,c4:0;n:type:ShaderForge.SFN_Multiply,id:5231,x:32459,y:32963,varname:node_5231,prsc:2|A-8288-OUT,B-4820-RGB,C-9759-RGB,D-8987-RGB;n:type:ShaderForge.SFN_SwitchProperty,id:6544,x:32689,y:32882,ptovrint:False,ptlb:node_6544,ptin:_node_6544,varname:node_6544,prsc:2,on:False|A-1897-OUT,B-5231-OUT;proporder:3209-4820-6544;pass:END;sub:END;*/

Shader "Shader Forge/SSS_light" {
    Properties {
        _RAMP ("RAMP", 2D) = "white" {}
        _node_4820 ("node_4820", Color) = (0.04411763,1,0.7626776,0)
        [MaterialToggle] _node_6544 ("node_6544", Float ) = 0.811626
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _RAMP; uniform float4 _RAMP_ST;
            uniform float4 _node_4820;
            uniform fixed _node_6544;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_2270 = float2((dot(i.normalDir,viewDirection)*0.8),((dot(i.normalDir,lightDirection)*0.3)+0.5));
                float4 node_8987 = tex2D(_RAMP,TRANSFORM_TEX(node_2270, _RAMP));
                float3 node_1897 = (node_8987.rgb*_LightColor0.rgb*attenuation);
                float3 finalColor = lerp( node_1897, (attenuation*_node_4820.rgb*_LightColor0.rgb*node_8987.rgb), _node_6544 );
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _RAMP; uniform float4 _RAMP_ST;
            uniform float4 _node_4820;
            uniform fixed _node_6544;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_2270 = float2((dot(i.normalDir,viewDirection)*0.8),((dot(i.normalDir,lightDirection)*0.3)+0.5));
                float4 node_8987 = tex2D(_RAMP,TRANSFORM_TEX(node_2270, _RAMP));
                float3 node_1897 = (node_8987.rgb*_LightColor0.rgb*attenuation);
                float3 finalColor = lerp( node_1897, (attenuation*_node_4820.rgb*_LightColor0.rgb*node_8987.rgb), _node_6544 );
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
