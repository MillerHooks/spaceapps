// Shader created with Shader Forge v1.06 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.06;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,dith:0,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:True,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.6965513,fgcg:0,fgcb:1,fgca:0.8196079,fgde:0.03,fgrn:0.09,fgrf:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:5813,x:33022,y:32700,varname:node_5813,prsc:2|emission-1864-RGB,alpha-6950-OUT;n:type:ShaderForge.SFN_Color,id:1864,x:32733,y:32656,ptovrint:False,ptlb:fres,ptin:_fres,varname:node_1864,prsc:2,glob:False,c1:0.1470588,c2:0.1529413,c3:1,c4:0.7019608;n:type:ShaderForge.SFN_Multiply,id:6950,x:32739,y:32946,varname:node_6950,prsc:2|A-3615-OUT,B-6803-OUT;n:type:ShaderForge.SFN_DepthBlend,id:6803,x:32739,y:32810,varname:node_6803,prsc:2|DIST-2856-OUT;n:type:ShaderForge.SFN_RemapRange,id:3615,x:32539,y:32946,varname:node_3615,prsc:2,frmn:0,frmx:1,tomn:0,tomx:0.1|IN-9620-OUT;n:type:ShaderForge.SFN_OneMinus,id:9620,x:32342,y:32946,varname:node_9620,prsc:2|IN-8813-OUT;n:type:ShaderForge.SFN_Fresnel,id:8813,x:32143,y:32946,varname:node_8813,prsc:2;n:type:ShaderForge.SFN_Slider,id:2856,x:32369,y:32831,ptovrint:False,ptlb:Depth Slider,ptin:_DepthSlider,varname:node_2856,prsc:2,min:0,cur:2.051282,max:5;proporder:1864-2856;pass:END;sub:END;*/

Shader "Shader Forge/Volume" {
    Properties {
        _fres ("fres", Color) = (0.1470588,0.1529413,1,0.7019608)
        _DepthSlider ("Depth Slider", Range(0, 5)) = 2.051282
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            Fog {Mode Global}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _fres;
            uniform float _DepthSlider;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 projPos : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = _fres.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,(((1.0 - (1.0-max(0,dot(normalDirection, viewDirection))))*0.1+0.0)*saturate((sceneZ-partZ)/_DepthSlider)));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
