// Shader created with Shader Forge v1.06 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.06;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,dith:0,ufog:False,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:True,fgoc:True,fgod:False,fgor:False,fgmd:2,fgcr:0.6012527,fgcg:0.3104455,fgcb:0.7279412,fgca:0.6588235,fgde:0,fgrn:0,fgrf:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:6545,x:33403,y:32595,varname:node_6545,prsc:2|diff-5280-OUT,spec-799-RGB,normal-1992-RGB,emission-8154-OUT,amdfl-6718-OUT,amspl-2083-OUT,difocc-2304-OUT,spcocc-5259-OUT,alpha-2616-OUT;n:type:ShaderForge.SFN_Slider,id:7935,x:32259,y:33313,ptovrint:False,ptlb:Slider,ptin:_Slider,varname:node_7935,prsc:2,min:0,cur:0.7264957,max:1;n:type:ShaderForge.SFN_Multiply,id:4534,x:32350,y:32802,varname:node_4534,prsc:2|A-7742-RGB,B-1915-OUT;n:type:ShaderForge.SFN_Color,id:7742,x:32112,y:32726,ptovrint:False,ptlb:GlowColor,ptin:_GlowColor,varname:_BaseColor_copy,prsc:2,glob:False,c1:0,c2:0.4145027,c3:0.9852941,c4:1;n:type:ShaderForge.SFN_Multiply,id:6736,x:32578,y:33114,varname:node_6736,prsc:2|A-6586-OUT,B-7935-OUT;n:type:ShaderForge.SFN_OneMinus,id:6586,x:32388,y:33114,varname:node_6586,prsc:2|IN-4473-OUT;n:type:ShaderForge.SFN_Fresnel,id:4473,x:32223,y:33114,varname:node_4473,prsc:2|NRM-9767-OUT,EXP-8951-OUT;n:type:ShaderForge.SFN_NormalVector,id:9767,x:32006,y:33036,prsc:2,pt:True;n:type:ShaderForge.SFN_ValueProperty,id:8951,x:32006,y:33229,ptovrint:False,ptlb:EXP,ptin:_EXP,varname:node_8951,prsc:2,glob:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:1915,x:32142,y:32912,ptovrint:False,ptlb:Glow_Intensity,ptin:_Glow_Intensity,varname:_EXP_copy,prsc:2,glob:False,v1:2.33;n:type:ShaderForge.SFN_Tex2d,id:6184,x:32101,y:31887,ptovrint:False,ptlb:BaseTexture,ptin:_BaseTexture,varname:node_6184,prsc:2,tex:5cce61f0ba60d4c86a272b33ddee8017,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Lerp,id:2853,x:32551,y:32802,varname:node_2853,prsc:2|A-6195-RGB,B-4534-OUT,T-6736-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:8154,x:32863,y:32510,ptovrint:False,ptlb:true/false,ptin:_truefalse,varname:node_8154,prsc:2,on:False|A-5280-OUT,B-2853-OUT;n:type:ShaderForge.SFN_Color,id:6195,x:32326,y:32612,ptovrint:False,ptlb:GlowColor_copy,ptin:_GlowColor_copy,varname:_GlowColor_copy,prsc:2,glob:False,c1:0.4824542,c2:0.9852941,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1992,x:32551,y:32631,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:_MainTexture_copy,prsc:2,tex:340c823bc55054f05b91267f8b3e8551,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:799,x:32539,y:32384,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:_MainTexture_copy_copy,prsc:2,tex:99466a3c70c904a80a3c506a43823d7c,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Binormal,id:4006,x:32101,y:32244,varname:node_4006,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5280,x:32336,y:32014,varname:node_5280,prsc:2|A-6184-RGB,B-6205-RGB;n:type:ShaderForge.SFN_Color,id:6205,x:32101,y:32079,ptovrint:False,ptlb:BaseColor,ptin:_BaseColor,varname:node_6205,prsc:2,glob:False,c1:0.1417492,c2:0.6470588,c3:0.1237024,c4:1;n:type:ShaderForge.SFN_Slider,id:6718,x:32762,y:32727,ptovrint:False,ptlb:Diff Light,ptin:_DiffLight,varname:node_6718,prsc:2,min:0,cur:1,max:4;n:type:ShaderForge.SFN_Slider,id:2304,x:32762,y:32897,ptovrint:False,ptlb:Diff Occ,ptin:_DiffOcc,varname:node_2304,prsc:2,min:0,cur:0,max:4;n:type:ShaderForge.SFN_Slider,id:5259,x:32762,y:32976,ptovrint:False,ptlb:Spec Occ,ptin:_SpecOcc,varname:node_5259,prsc:2,min:0,cur:0,max:4;n:type:ShaderForge.SFN_Slider,id:2083,x:32762,y:32811,ptovrint:False,ptlb:Spec Light,ptin:_SpecLight,varname:node_2083,prsc:2,min:0,cur:0,max:4;n:type:ShaderForge.SFN_Slider,id:2616,x:32762,y:32643,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_2616,prsc:2,min:0,cur:1,max:1;proporder:7935-7742-8951-1915-8154-6195-1992-799-6205-6718-2304-5259-2083-6184-2616;pass:END;sub:END;*/

Shader "Shader Forge/Glow" {
    Properties {
        _Slider ("Slider", Range(0, 1)) = 0.7264957
        _GlowColor ("GlowColor", Color) = (0,0.4145027,0.9852941,1)
        _EXP ("EXP", Float ) = 1
        _Glow_Intensity ("Glow_Intensity", Float ) = 2.33
        [MaterialToggle] _truefalse ("true/false", Float ) = 0
        _GlowColor_copy ("GlowColor_copy", Color) = (0.4824542,0.9852941,0,1)
        _Normal ("Normal", 2D) = "bump" {}
        _Specular ("Specular", 2D) = "white" {}
        _BaseColor ("BaseColor", Color) = (0.1417492,0.6470588,0.1237024,1)
        _DiffLight ("Diff Light", Range(0, 4)) = 1
        _DiffOcc ("Diff Occ", Range(0, 4)) = 0
        _SpecOcc ("Spec Occ", Range(0, 4)) = 0
        _SpecLight ("Spec Light", Range(0, 4)) = 0
        _BaseTexture ("BaseTexture", 2D) = "black" {}
        _Alpha ("Alpha", Range(0, 1)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _Slider;
            uniform float4 _GlowColor;
            uniform float _EXP;
            uniform float _Glow_Intensity;
            uniform sampler2D _BaseTexture; uniform float4 _BaseTexture_ST;
            uniform fixed _truefalse;
            uniform float4 _GlowColor_copy;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Specular; uniform float4 _Specular_ST;
            uniform float4 _BaseColor;
            uniform float _DiffLight;
            uniform float _DiffOcc;
            uniform float _SpecOcc;
            uniform float _SpecLight;
            uniform float _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                #ifndef LIGHTMAP_OFF
                    float4 uvLM : TEXCOORD5;
                #else
                    float3 shLight : TEXCOORD5;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularAO = float3(_SpecOcc,_SpecOcc,_SpecOcc);
                float4 _Specular_var = tex2D(_Specular,TRANSFORM_TEX(i.uv0, _Specular));
                float3 specularColor = _Specular_var.rgb;
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow);
                float3 indirectSpecular = (0 + float3(_SpecLight,_SpecLight,_SpecLight)) * specularAO;
                float3 specular = (directSpecular + indirectSpecular) * specularColor;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 indirectDiffuse = float3(0,0,0);
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                indirectDiffuse += float3(_DiffLight,_DiffLight,_DiffLight); // Diffuse Ambient Light
                indirectDiffuse *= float3(_DiffOcc,_DiffOcc,_DiffOcc); // Diffuse AO
                float4 _BaseTexture_var = tex2D(_BaseTexture,TRANSFORM_TEX(i.uv0, _BaseTexture));
                float3 node_5280 = (_BaseTexture_var.rgb*_BaseColor.rgb);
                float3 diffuse = (directDiffuse + indirectDiffuse) * node_5280;
////// Emissive:
                float3 emissive = lerp( node_5280, lerp(_GlowColor_copy.rgb,(_GlowColor.rgb*_Glow_Intensity),((1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),_EXP))*_Slider)), _truefalse );
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                return fixed4(finalColor,_Alpha);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _Slider;
            uniform float4 _GlowColor;
            uniform float _EXP;
            uniform float _Glow_Intensity;
            uniform sampler2D _BaseTexture; uniform float4 _BaseTexture_ST;
            uniform fixed _truefalse;
            uniform float4 _GlowColor_copy;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Specular; uniform float4 _Specular_ST;
            uniform float4 _BaseColor;
            uniform float _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                #ifndef LIGHTMAP_OFF
                    float4 uvLM : TEXCOORD7;
                #else
                    float3 shLight : TEXCOORD7;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float4 _Specular_var = tex2D(_Specular,TRANSFORM_TEX(i.uv0, _Specular));
                float3 specularColor = _Specular_var.rgb;
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow);
                float3 specular = directSpecular * specularColor;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _BaseTexture_var = tex2D(_BaseTexture,TRANSFORM_TEX(i.uv0, _BaseTexture));
                float3 node_5280 = (_BaseTexture_var.rgb*_BaseColor.rgb);
                float3 diffuse = directDiffuse * node_5280;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * _Alpha,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
