// Shader created with Shader Forge v1.06 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.06;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:1,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:True,dith:0,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.6103804,fgcg:0.1211073,fgcb:0.8235294,fgca:0.6588235,fgde:0,fgrn:0,fgrf:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:6892,x:32719,y:32712,varname:node_6892,prsc:2|normal-1999-RGB,emission-3996-OUT,custl-7590-OUT;n:type:ShaderForge.SFN_Tex2d,id:1999,x:32730,y:32501,ptovrint:False,ptlb:Normal Map,ptin:_NormalMap,varname:node_1999,prsc:2,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Add,id:3996,x:31967,y:32545,varname:node_3996,prsc:2|A-3346-OUT,B-3204-OUT;n:type:ShaderForge.SFN_Multiply,id:3204,x:31755,y:32545,varname:node_3204,prsc:2|A-6141-RGB,B-3822-OUT;n:type:ShaderForge.SFN_AmbientLight,id:6141,x:31527,y:32545,varname:node_6141,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3822,x:31304,y:32673,varname:node_3822,prsc:2|A-6671-RGB,B-9379-RGB;n:type:ShaderForge.SFN_Lerp,id:3346,x:31630,y:32167,varname:node_3346,prsc:2|A-2685-OUT,B-7916-OUT,T-9007-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9007,x:31527,y:32343,ptovrint:False,ptlb:Rim Power,ptin:_RimPower,varname:node_9007,prsc:2,glob:False,v1:0;n:type:ShaderForge.SFN_Vector1,id:2685,x:31527,y:32097,varname:node_2685,prsc:2,v1:0;n:type:ShaderForge.SFN_Blend,id:7916,x:31409,y:32167,varname:node_7916,prsc:2,blmd:0,clmp:True|SRC-1023-OUT,DST-3559-RGB;n:type:ShaderForge.SFN_Multiply,id:1023,x:31188,y:32170,varname:node_1023,prsc:2|A-1043-OUT,B-6271-RGB,C-2827-RGB;n:type:ShaderForge.SFN_Tex2d,id:3559,x:31188,y:32339,ptovrint:False,ptlb:RIM Mask,ptin:_RIMMask,varname:node_3559,prsc:2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Fresnel,id:1043,x:30944,y:32047,varname:node_1043,prsc:2|EXP-4170-OUT;n:type:ShaderForge.SFN_Exp,id:4170,x:30734,y:32040,varname:node_4170,prsc:2,et:1|IN-7853-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7853,x:30734,y:32223,ptovrint:False,ptlb:Rim Thikness,ptin:_RimThikness,varname:_RimPower_copy,prsc:2,glob:False,v1:0;n:type:ShaderForge.SFN_Cubemap,id:6271,x:30944,y:32213,ptovrint:False,ptlb:Rim Cubemap,ptin:_RimCubemap,varname:node_6271,prsc:2,cube:2f821dbbb5e173e468876ef2e4eaa490,pvfc:0;n:type:ShaderForge.SFN_Color,id:2827,x:30942,y:32393,ptovrint:False,ptlb:Rim Color,ptin:_RimColor,varname:node_2827,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:6671,x:30946,y:32575,ptovrint:False,ptlb:Diffuse Color,ptin:_DiffuseColor,varname:node_6671,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:9379,x:30946,y:32764,ptovrint:False,ptlb:Diffuse Texture,ptin:_DiffuseTexture,varname:node_9379,prsc:2,tex:6205c27cc031f4e66b8ea90d1bfaa158,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:753,x:31470,y:32851,varname:node_753,prsc:2|A-9379-RGB,B-1542-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:7938,x:30946,y:32956,varname:node_7938,prsc:2;n:type:ShaderForge.SFN_Relay,id:669,x:31238,y:32990,varname:node_669,prsc:2;n:type:ShaderForge.SFN_Multiply,id:692,x:31418,y:33082,varname:node_692,prsc:2|A-669-OUT,B-1542-OUT;n:type:ShaderForge.SFN_Append,id:2463,x:31613,y:33082,varname:node_2463,prsc:2|A-692-OUT,B-692-OUT;n:type:ShaderForge.SFN_Dot,id:1542,x:31209,y:33082,varname:node_1542,prsc:2,dt:0|A-2890-OUT;n:type:ShaderForge.SFN_NormalVector,id:2890,x:30946,y:33104,prsc:2,pt:False;n:type:ShaderForge.SFN_LightVector,id:3835,x:30946,y:33275,varname:node_3835,prsc:2;n:type:ShaderForge.SFN_HalfVector,id:5841,x:30946,y:33421,varname:node_5841,prsc:2;n:type:ShaderForge.SFN_Dot,id:8546,x:31209,y:33311,varname:node_8546,prsc:2,dt:0|A-2890-OUT;n:type:ShaderForge.SFN_Tex2d,id:9292,x:31801,y:33082,ptovrint:False,ptlb:Ramp 2,ptin:_Ramp2,varname:node_9292,prsc:2,tex:181ec605fb3b4314bbee61db18ab41c0,ntxv:0,isnm:False|UVIN-2463-OUT;n:type:ShaderForge.SFN_Add,id:5590,x:31969,y:33082,varname:node_5590,prsc:2|A-9292-RGB,B-753-OUT;n:type:ShaderForge.SFN_Add,id:2954,x:32157,y:33134,varname:node_2954,prsc:2|A-5590-OUT,B-1713-OUT,C-1820-OUT;n:type:ShaderForge.SFN_Multiply,id:7590,x:32389,y:33082,cmnt:Final Lightning,varname:node_7590,prsc:2|A-2954-OUT,B-3822-OUT,C-2531-OUT;n:type:ShaderForge.SFN_Relay,id:2531,x:32028,y:32955,varname:node_2531,prsc:2;n:type:ShaderForge.SFN_LightColor,id:7518,x:32389,y:33244,varname:node_7518,prsc:2;n:type:ShaderForge.SFN_Power,id:528,x:31417,y:33313,varname:node_528,prsc:2|VAL-8546-OUT,EXP-9645-OUT;n:type:ShaderForge.SFN_Exp,id:9645,x:31422,y:33461,varname:node_9645,prsc:2,et:1|IN-7255-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7255,x:31422,y:33649,ptovrint:False,ptlb:Gloss 1,ptin:_Gloss1,varname:node_7255,prsc:2,glob:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:1713,x:31801,y:33313,varname:node_1713,prsc:2|A-528-OUT,B-3223-RGB,C-5997-RGB,D-5048-OUT;n:type:ShaderForge.SFN_Tex2d,id:3223,x:31607,y:33405,ptovrint:False,ptlb:Specular Map,ptin:_SpecularMap,varname:node_3223,prsc:2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:5997,x:31607,y:33605,ptovrint:False,ptlb:Specular Color,ptin:_SpecularColor,varname:node_5997,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:5048,x:31801,y:33486,ptovrint:False,ptlb:Specularity,ptin:_Specularity,varname:_Glosiness_copy,prsc:2,glob:False,v1:0;n:type:ShaderForge.SFN_Cubemap,id:1827,x:31607,y:33814,ptovrint:False,ptlb:Reflection Cubemap,ptin:_ReflectionCubemap,varname:node_1827,prsc:2,cube:2f821dbbb5e173e468876ef2e4eaa490,pvfc:0;n:type:ShaderForge.SFN_Tex2d,id:8377,x:31607,y:34005,ptovrint:False,ptlb:Reflection Map,ptin:_ReflectionMap,varname:node_8377,prsc:2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1820,x:31852,y:33902,varname:node_1820,prsc:2|A-1827-RGB,B-1827-A,C-5323-OUT,D-8377-RGB;n:type:ShaderForge.SFN_ValueProperty,id:5323,x:31852,y:34056,ptovrint:False,ptlb:Reflectiveness,ptin:_Reflectiveness,varname:_Glosiness_copy,prsc:2,glob:False,v1:1;proporder:1999-9007-3559-7853-6271-2827-9379-6671-9292-7255-3223-5997-5048-1827-8377-5323;pass:END;sub:END;*/

Shader "Custom/TestShader" {
    Properties {
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _RimPower ("Rim Power", Float ) = 0
        _RIMMask ("RIM Mask", 2D) = "white" {}
        _RimThikness ("Rim Thikness", Float ) = 0
        _RimCubemap ("Rim Cubemap", Cube) = "_Skybox" {}
        _RimColor ("Rim Color", Color) = (1,1,1,1)
        _DiffuseTexture ("Diffuse Texture", 2D) = "white" {}
        _DiffuseColor ("Diffuse Color", Color) = (1,1,1,1)
        _Ramp2 ("Ramp 2", 2D) = "white" {}
        _Gloss1 ("Gloss 1", Float ) = 0
        _SpecularMap ("Specular Map", 2D) = "white" {}
        _SpecularColor ("Specular Color", Color) = (0.5,0.5,0.5,1)
        _Specularity ("Specularity", Float ) = 0
        _ReflectionCubemap ("Reflection Cubemap", Cube) = "_Skybox" {}
        _ReflectionMap ("Reflection Map", 2D) = "white" {}
        _Reflectiveness ("Reflectiveness", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "PrePassBase"
            Tags {
                "LightMode"="PrePassBase"
            }
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_PREPASSBASE
            #include "UnityCG.cginc"
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform fixed4 unity_Ambient;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float3 tangentDir : TEXCOORD2;
                float3 binormalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Vectors:
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                return fixed4( normalDirection * 0.5 + 0.5, max(0.5,0.0078125) );
            }
            ENDCG
        }
        Pass {
            Name "PrePassFinal"
            Tags {
                "LightMode"="PrePassFinal"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_PREPASSFINAL
            #include "UnityCG.cginc"
            #pragma multi_compile_prepassfinal
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _LightBuffer;
            #if defined (SHADER_API_XBOX360) && defined (HDR_LIGHT_PREPASS_ON)
                sampler2D _LightSpecBuffer;
            #endif
            uniform fixed4 unity_Ambient;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _RimPower;
            uniform sampler2D _RIMMask; uniform float4 _RIMMask_ST;
            uniform float _RimThikness;
            uniform samplerCUBE _RimCubemap;
            uniform float4 _RimColor;
            uniform float4 _DiffuseColor;
            uniform sampler2D _DiffuseTexture; uniform float4 _DiffuseTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float node_2685 = 0.0;
                float4 _RIMMask_var = tex2D(_RIMMask,TRANSFORM_TEX(i.uv0, _RIMMask));
                float4 _DiffuseTexture_var = tex2D(_DiffuseTexture,TRANSFORM_TEX(i.uv0, _DiffuseTexture));
                float3 node_3822 = (_DiffuseColor.rgb*_DiffuseTexture_var.rgb);
                float3 emissive = (lerp(float3(node_2685,node_2685,node_2685),saturate(min((pow(1.0-max(0,dot(normalDirection, viewDirection)),exp2(_RimThikness))*texCUBE(_RimCubemap,viewReflectDirection).rgb*_RimColor.rgb),_RIMMask_var.rgb)),_RimPower)+(UNITY_LIGHTMODEL_AMBIENT.rgb*node_3822));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _RimPower;
            uniform sampler2D _RIMMask; uniform float4 _RIMMask_ST;
            uniform float _RimThikness;
            uniform samplerCUBE _RimCubemap;
            uniform float4 _RimColor;
            uniform float4 _DiffuseColor;
            uniform sampler2D _DiffuseTexture; uniform float4 _DiffuseTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float node_2685 = 0.0;
                float4 _RIMMask_var = tex2D(_RIMMask,TRANSFORM_TEX(i.uv0, _RIMMask));
                float4 _DiffuseTexture_var = tex2D(_DiffuseTexture,TRANSFORM_TEX(i.uv0, _DiffuseTexture));
                float3 node_3822 = (_DiffuseColor.rgb*_DiffuseTexture_var.rgb);
                float3 emissive = (lerp(float3(node_2685,node_2685,node_2685),saturate(min((pow(1.0-max(0,dot(normalDirection, viewDirection)),exp2(_RimThikness))*texCUBE(_RimCubemap,viewReflectDirection).rgb*_RimColor.rgb),_RIMMask_var.rgb)),_RimPower)+(UNITY_LIGHTMODEL_AMBIENT.rgb*node_3822));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
