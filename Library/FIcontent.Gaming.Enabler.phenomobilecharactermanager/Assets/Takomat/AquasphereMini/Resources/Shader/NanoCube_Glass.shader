// Shader created with Shader Forge v1.06 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.06;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,dith:0,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.8235294,fgcg:0.1211073,fgcb:0.1211073,fgca:1,fgde:0.04,fgrn:0.45,fgrf:2,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:6895,x:33161,y:32551,varname:node_6895,prsc:2|normal-130-RGB,custl-9429-OUT,alpha-130-A;n:type:ShaderForge.SFN_LightVector,id:6738,x:32035,y:32845,varname:node_6738,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:9500,x:32035,y:32690,prsc:2,pt:False;n:type:ShaderForge.SFN_ViewReflectionVector,id:666,x:32035,y:32965,varname:node_666,prsc:2;n:type:ShaderForge.SFN_Dot,id:9751,x:32264,y:32792,varname:node_9751,prsc:2,dt:0|A-9500-OUT,B-6738-OUT;n:type:ShaderForge.SFN_Dot,id:7743,x:32264,y:32965,varname:node_7743,prsc:2,dt:1|A-6738-OUT,B-666-OUT;n:type:ShaderForge.SFN_Color,id:9477,x:32264,y:32604,ptovrint:False,ptlb:LightColor,ptin:_LightColor,varname:_node_3337_copy,prsc:2,glob:False,c1:0,c2:0.751724,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:9201,x:32490,y:32705,varname:node_9201,prsc:2|A-9477-A,B-9751-OUT;n:type:ShaderForge.SFN_Power,id:2872,x:32503,y:32894,varname:node_2872,prsc:2|VAL-7743-OUT,EXP-737-OUT;n:type:ShaderForge.SFN_Exp,id:737,x:32264,y:33306,varname:node_737,prsc:2,et:1|IN-5223-OUT;n:type:ShaderForge.SFN_Slider,id:5223,x:31878,y:33310,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_5223,prsc:2,min:1,cur:7.538566,max:11;n:type:ShaderForge.SFN_Add,id:5253,x:32697,y:32776,varname:node_5253,prsc:2|A-9201-OUT,B-2872-OUT,C-4213-OUT;n:type:ShaderForge.SFN_LightColor,id:2165,x:32697,y:32942,varname:node_2165,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9429,x:32940,y:32798,varname:node_9429,prsc:2|A-5253-OUT,B-2165-A,C-6700-OUT;n:type:ShaderForge.SFN_Fresnel,id:6700,x:32687,y:33518,varname:node_6700,prsc:2|NRM-8728-RGB,EXP-6210-OUT;n:type:ShaderForge.SFN_Exp,id:6210,x:32509,y:33614,varname:node_6210,prsc:2,et:0|IN-9354-OUT;n:type:ShaderForge.SFN_Tex2d,id:8728,x:32509,y:33440,ptovrint:False,ptlb:Grey,ptin:_Grey,varname:node_8728,prsc:2,tex:5de0eafe0c281495b8272d9a1d7c3ea8,ntxv:3,isnm:False;n:type:ShaderForge.SFN_Slider,id:9354,x:32186,y:33598,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:_Fresnel_copy,prsc:2,min:0,cur:1.062212,max:1.5;n:type:ShaderForge.SFN_AmbientLight,id:2061,x:32633,y:32339,varname:node_2061,prsc:2;n:type:ShaderForge.SFN_Color,id:130,x:32864,y:32515,ptovrint:False,ptlb:MainColor,ptin:_MainColor,varname:node_130,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:0.1686275;n:type:ShaderForge.SFN_Binormal,id:3038,x:32035,y:33104,varname:node_3038,prsc:2;n:type:ShaderForge.SFN_Dot,id:4213,x:32264,y:33132,varname:node_4213,prsc:2,dt:0|A-666-OUT,B-3038-OUT;proporder:9477-5223-8728-9354-130;pass:END;sub:END;*/

Shader "Shader Forge/NanoCube_Glass" {
    Properties {
        _LightColor ("LightColor", Color) = (0,0.751724,1,1)
        _Gloss ("Gloss", Range(1, 11)) = 7.538566
        _Grey ("Grey", 2D) = "bump" {}
        _Fresnel ("Fresnel", Range(0, 1.5)) = 1.062212
        _MainColor ("MainColor", Color) = (1,1,1,0.1686275)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _LightColor;
            uniform float _Gloss;
            uniform sampler2D _Grey; uniform float4 _Grey_ST;
            uniform float _Fresnel;
            uniform float4 _MainColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalLocal = _MainColor.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float4 _Grey_var = tex2D(_Grey,TRANSFORM_TEX(i.uv0, _Grey));
                float node_9429 = (((_LightColor.a*dot(i.normalDir,lightDirection))+pow(max(0,dot(lightDirection,viewReflectDirection)),exp2(_Gloss))+dot(viewReflectDirection,i.binormalDir))*_LightColor0.a*pow(1.0-max(0,dot(_Grey_var.rgb, viewDirection)),exp(_Fresnel)));
                float3 finalColor = float3(node_9429,node_9429,node_9429);
                return fixed4(finalColor,_MainColor.a);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _LightColor;
            uniform float _Gloss;
            uniform sampler2D _Grey; uniform float4 _Grey_ST;
            uniform float _Fresnel;
            uniform float4 _MainColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalLocal = _MainColor.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float4 _Grey_var = tex2D(_Grey,TRANSFORM_TEX(i.uv0, _Grey));
                float node_9429 = (((_LightColor.a*dot(i.normalDir,lightDirection))+pow(max(0,dot(lightDirection,viewReflectDirection)),exp2(_Gloss))+dot(viewReflectDirection,i.binormalDir))*_LightColor0.a*pow(1.0-max(0,dot(_Grey_var.rgb, viewDirection)),exp(_Fresnel)));
                float3 finalColor = float3(node_9429,node_9429,node_9429);
                return fixed4(finalColor * _MainColor.a,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
