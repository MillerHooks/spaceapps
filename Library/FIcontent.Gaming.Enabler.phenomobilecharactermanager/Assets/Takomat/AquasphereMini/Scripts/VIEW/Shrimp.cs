﻿//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Epigene;

namespace AquaSphereMini.VIEW
{
	/// <summary>
	/// ResourceViewShrimp is the View of a Resource
	/// </summary>
	public class Shrimp : Epigene.ResourceView
	{
		private MonoBehaviour behaviour; 

		private AquaSphere.MODEL.C2000_Shrimp model;
		private GameObject shrimp;

		//DEAD, SICK, HEALTHY, AAAVITALITY};
		private int modelHealth;
		
		public Shrimp ()
		{
			// handle specific character shrimp
			// access the shader of the shrimp
			shrimp = GameObject.Find("model_C2010_SHRIMP_CrystelRed");
			modelHealth = 1;
			Set (null);
		}

		public void Set(AquaSphere.MODEL.C2000_Shrimp _model)
		{
			model = _model;	
			UpdateState();
		}

		public void UpdateState()
		{
			if(model!=null)
			{  modelHealth = (int) model.Health; }
			
			//Log.Assert(shrimp!=null, 
			// "Missing shrimp configuration in CharacterManager.cs!");
			// Debug.Log ("//////// --- Start accessing -------");
			setViewState(modelHealth);
		}



		private void setViewState(int modelHealth)
		{
			if(shrimp==null) return;
		
			// Debug.Log ("//////// --- shrimp found -------");
			Renderer meshRenderer = shrimp.GetComponent<Renderer>();
			//meshRenderer.material.SetFloat ("Alpha", 0.0F);

			// Health states are:
			//       {DEAD, SICK, HEALTHY, AAAVITALITY};
			//         0      1      2        3
			// here SICK = 0 , HEALTHY = 1, dead ?
			int modelHealthMapped = modelHealth-1;
			
			float alpha = meshRenderer.materials[modelHealthMapped].GetFloat ("_Alpha");
			
			// Debug.Log ("//////// --- alpha of the shrimp renderer -------"+
			// alpha+ "modelHealthMapped" + modelHealthMapped);
			meshRenderer.materials[modelHealthMapped].SetFloat ("_Alpha", 0.9F);
			
			alpha = meshRenderer.materials[modelHealthMapped].GetFloat ("_Alpha");
			
			// Debug.Log ("//////// --- alpha of the shrimp renderer -------"+alpha);
			
			// put the rest of the possible health views to zero;-)
			// we know, that this can only be one meteri
			
			for (int i = 0; i < meshRenderer.materials.Length; i++) 
			{
				if(modelHealthMapped!=i)
					meshRenderer.materials[i].SetFloat ("_Alpha", 0.0F);
			}

			//float alpha2 = meshRenderer.materials[1].GetFloat ("_Alpha");
			
			//Debug.Log ("//////// --- alpha of the shrimp renderer -------"+alpha2);
			
			
			//alpha2 = meshRenderer.materials[1].GetFloat ("_Alpha");
			
			//Debug.Log ("//////// --- alpha of the shrimp renderer -------"+alpha2);
			
			
			// ;.GetFloat ("Alpha");
			//.color.a = 0;
			//color.shader = Shader.Find("Takomat/Timeline");
			//shrimp.renderer.material.color.a
		}
	}
}//namespace