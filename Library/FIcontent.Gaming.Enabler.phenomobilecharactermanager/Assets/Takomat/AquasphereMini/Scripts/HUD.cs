//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Epigene;
using Epigene.UI;
using Epigene.GAME;
using Epigene.MODEL;
using Epigene.AUDIO;
using Epigene.VIEW;

using AquaSphere.IO;


namespace AquaSphereMini
{
	///<summary>
	/// This class is responsible for create and manage the HUD menu system.
	/// It uses multiple sub menus and show/activate the one based on which
	/// is the activate menu at the moment.
	/// The users must click on the corrsponting menu button in the hud
	/// to activate the submenu.
	///</summary>
	public class HUD : MonoBehaviour 
	{

		///ui elements to bind to this class
		//public 	GameObject 	mainMenu;

		/// <summary>
		/// This tells the camera is one of the submenus is open
		/// </summary>
		public static bool	hudOpen;

		/// <summary>
		/// The user interface manager.
		/// </summary>
		public UIManager uiManager;
		
		/// <summary>
		/// Player manager
		/// </summary>
		public GameManager gpm;
		
		//private F0000_FinanceManager financeManager;
		
		///<summary>
		/// predefined states for submenus
		/// NONE: no submenu
		/// BUILD: show build submenu
		/// FINANCE: show finance submenu
		///</summary>
		private enum SubMenu {NONE, FLORA, ENVIRONMENT, FAUNA, BACTERIA};
		private SubMenu activeSubmenu = SubMenu.NONE;

		private bool mouseDown = false;
		
		/// <summary>
		/// Camera layer mask to save current mask
		/// to temporarly disable the screen
		/// </summary>
		private int lastLayerMask;
		
		void LogCyan(string _first, string _second)
		{
			#if DEBUG
			string logstr = "No <color=cyan>";
			logstr += _first;
			logstr += "</color> assigned to:<color=cyan>";
			logstr += _second;
			logstr += "</color>";			
			Debug.Log (logstr);
			#endif
		}
		//------------------------------------------------------------------------------
		///<summary>
		///
		///</summary>
		void Awake()
		{
			Log.Debug(gameObject.name+" AWAKEN");
			
			//get managers
			uiManager = UIManager.Instance;
			gpm = GameManager.Instance;
			//financeManager = F0000_FinanceManager.Instance;
			
			//TODO dynamic creation from file		
			//if (mainMenu == null) LogCyan("mainMenu", gameObject.name);

			hudOpen = false;
		}//Awake()
		
		/// <summary>
		/// Enable the HUD will register ui elements
		/// </summary>
		void OnEnable()
		{
			Log.Debug(gameObject.name+" Enabled");

			//mainMenu.SetActive(true);
			//register event handler			
			GameManager.Instance.RegisterEventHandler("SCREEN", ProcessScreenEvent);
			GameManager.Instance.RegisterEventHandler("DIALOG", ProcessDialogEvent);	
			GameManager.Instance.RegisterEventHandler("INIT", ProcessInitEvent);
			GameManager.Instance.RegisterEventHandler("NANOCUBE", ProcessBuildEvent);


		}//OnEnable()


		
		/// <summary>
		/// Disable the hud will remove registered ui elements
		/// </summary>
		void OnDisable()
		{
			Log.Debug(gameObject.name+" Disabled");

			GameManager.Instance.RemoveEventHandler("SCREEN", ProcessScreenEvent);
			GameManager.Instance.RemoveEventHandler("DIALOG", ProcessDialogEvent);
			GameManager.Instance.RemoveEventHandler("INIT", ProcessInitEvent);
			GameManager.Instance.RemoveEventHandler("NANOCUBE", ProcessBuildEvent);
	

		}//OnDisable()
		
		/// <summary>
		/// Start up
		/// </summary>
		void Start()
		{
			Log.Debug(gameObject.name + " Start");

		}//Start()
		
//------------------------------------------------------------------------------
		/// <summary>
		/// Function to check new events.
		/// </summary>
		public void ProcessScreenEvent(string _eventId, string _param)
		{
			string eventIdP0006_balance = "P0006_balance";
			
			//Log.GameTimes(
            // eventType+ ": EVENT("+eventType+"): " + _eventId+","+_param);
			
			
			if (eventIdP0006_balance == _eventId) 
			{
				SetBalance(_param);
			}			
		} 

		public void SetBalance(string balance)
		{
				GameObject obj = GameObject.Find ("SimulationMenu/HUD/HUD.003");
				// Log.Assert(obj,
                // "Missing Finance.currentWaterRate in" + gameObject.name);
				
				//Log.Debug("FIRE : "+id);
				//menu = GameObject.Find("HUD/menu/HUD.003");
				Log.Assert(obj != null, "Cannot find HUD.003 balance uitext!");
				if (obj != null) 
				{
					//Log.Assert(menu, "Missing menu in "+gameObject.name);
					// else 
					UIText currentBalance = obj.GetComponent<UIText> ();
					
					// Format must be done in the editor attributes
					//currentBalance.format = "{0:F2} €";
					currentBalance.Text = balance;
				}
		}
		
		/// <summary>
		/// Function to check new events.
		/// </summary>
		public void ProcessDialogEvent(string _eventId, string _param)
		{
			DialogManager.Instance.HUD (_eventId, _param);
		} 

		/// <summary>
		/// Function to load in Library Files
		/// </summary>
		/// <param name="_eventId">_event identifier.</param>
		/// <param name="_param">_param.</param>
		public void ProcessInitEvent(string _eventId, string _param)
		{
			if (_eventId != "Library") return;
			WebPlayerDebugManager.addOutput(
                "INIT Event: " + _eventId + ", " + _param, 4);

			WebPlayerDebugManager.addOutput("Library initialised." ,1);
		}

		public void ProcessBuildEvent(string eventId, string param)
		{
			CloseAllSubmenus();
		}

//------------------------------------------------------------------------------
		
		/// <summary>
		/// Toggles the specific Submenus depending on Button Name
		/// </summary>
		/// <param name="button">Button.</param>
		public void Toggle(string button)
		{
		}
		
		/// <summary>
		/// Closes all submenus.
		/// </summary>
		public void CloseAllSubmenus()
		{
			activeSubmenu = SubMenu.NONE;
		}
		
		/// <summary>
		/// Build a construction.
		/// This function is called when the user clicks
		/// on the "BUILD" button in the build selection panel.
		/// The function will change the icon to the selected
		/// consturction model and will hide the build panels.
		/// Parameter is the name of the button.
		/// </summary>
		public void BuildConstruction(string param)
		{
			CloseAllSubmenus();

			Debug.Log("<color=yellow> ++++++++   build const</color>");
			
			if(param == "btn_Build_1")
			{
				GameManager.Instance.Event(
					"NANOCUBE",
					"NANOCUBE",
					"addSoilShrimpGravel");
			}
			else if (param == "btn_Build_2")
			{
				GameManager.Instance.Event(
					"NANOCUBE",
					"NANOCUBE",
					"addSoilDeponit");
			}
			else if (param == "btn_Build_3")
			{
				GameManager.Instance.Event(
					"NANOCUBE",
					"NANOCUBE",
					"addWater");
			}
			else if (param == "btn_FloraBuild_1")
			{
				GameManager.Instance.Event(
					"NANOCUBE",
					"NANOCUBE",
					"addLudwigia");
			}
			else if (param == "btn_FaunaBuild_1")
			{
				GameManager.Instance.Event(
					"NANOCUBE",
					"NANOCUBE",
					"addRedBee");
			}
		}//BuildConstruction()


		/// <summary>
		/// Process store events
		/// </summary>
		public void ProcessStoreEvent(string eventId, string data)
		{
			switch(eventId)
			{
				case "CURRENCY_BALANCE_CHANGED":
					int balance = System.Convert.ToInt32(data);
					Debug.Log("<color=yellow>* STORE Balance:</color>"+balance);
					// UIText text = (UIText)GetObject("Balance");
					// text.Text = "Balance: " + balance.ToString();
					SetBalance(data);
					break;
			}
		}

	}//class HUD
}//namespace
//------------------------------------------------------------------------------