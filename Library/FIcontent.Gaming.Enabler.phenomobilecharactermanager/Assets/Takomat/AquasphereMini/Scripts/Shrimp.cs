﻿//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

using Epigene.GAME;

namespace AquaSphereMini
{
public class Shrimp : MonoBehaviour {

	/// <summary>
	/// Character manager class to manage a character
	/// 
	/// Current Implementation : 
	///  Currently it is for one 3D character and the instance is used 
	///  multiple times for different characters
	/// 
	/// Current character code design
	/// 2D character : UICharacter(View) has a NPC(Model)
	/// 3D character : 
	///   CharacterManager(Model) and has the view link 
	///   to the 3D model as a GameObject(View)
	/// Basically linking in the model the view or vice 
	/// versa is used for update code 
	/// 
	/// </summary>

//------------------------------------------------------------------------------

	//current speed range between min/maxSpeed
	public float speed        = 0f;	
	//current direction, range between -1 and +1
	public float direction    = 0;	

	public float aiSpeed      = 1.0f; //how quickly should AI update in sec
	public float acceleration = 0.4f; //how much to modify on speed
	public float rotation     = 0.1f; //how much to mdofiy on direction

	public float maxSpeed     = 1;
	public float minSpeed     = -0.2f;

	private float turningRate = 180;
	private float slast = 0.1234f;

    private AquaSphereMini.VIEW.Shrimp view;
    private AquaSphere.MODEL.C2000_Shrimp model;

	//------------------------------------------------------------------------------

	// Use this for initialization
	void OnEnable() 
	{
		GameManager.Instance.RegisterEventHandler(
			"CHARACTER", ProcessCharacterEvent);
		
	    model = new AquaSphere.MODEL.C2000_Shrimp();
		model.Health = AquaSphere.MODEL.HealthType.HEALTHY;
		int healthSICK = (int) AquaSphere.MODEL.HealthType.HEALTHY;
		Debug.Log ("//////// --- AquaSphere.MODEL.HealthType.SICK : "+ healthSICK);

		view  = new AquaSphereMini.VIEW.Shrimp();
		view.Set(model);

		if(!GetComponent<Animation>().isPlaying)
		{
			GetComponent<Animation>().Play();
		}
		
		ExecuteAI();
	}

//------------------------------------------------------------------------------
	void OnDisable()
	{
		GameManager.Instance.RemoveEventHandler(
			"CHARACTER", ProcessCharacterEvent);

		if(GetComponent<Animation>() != null && 
		   GetComponent<Animation>().isPlaying)
		{
			GetComponent<Animation>().Stop();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(direction > 1) direction = 1;
		if(direction < -1) direction = -1;
		if(speed > maxSpeed) speed = maxSpeed;
		if(speed < minSpeed) speed = minSpeed;

		UpdateAnimation();
		Update3DModel();
	
	}

	///<summary>
	/// This function will update the speed of animation,
	/// so it will give better match with current moves.
	///</summary>
	private void UpdateAnimation()
	{
		//calculate real anim speed, since we never want to really stop it
		float s = (speed == 0) ? 0.05f : speed;
		if(s < 0.1 && direction != 0)
		{
			s = 0.2f;
		    model.Health = AquaSphere.MODEL.HealthType.SICK;
		}

		if(slast!=s)
		{
				if(s==0.2f) 

				{
					Debug.Log("SWITCH");
					model.Health = AquaSphere.MODEL.HealthType.HEALTHY;
					view.UpdateState();
				}
				else 
				{
					Debug.Log("SWITCH SICK");
					model.Health = AquaSphere.MODEL.HealthType.SICK;
					view.UpdateState();
				}

		}
		slast = s;

		//update each animation's speed
		foreach (AnimationState state in GetComponent<Animation>()) 
		{
            state.speed = s * 0.8f;
        }
	}

	// obj.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");

	///<summary>
	/// This function will update the model
	/// with position and rotation by speed and direction.
	///</summary>
	private void Update3DModel()
	{
		//move ahead by speed
		transform.Translate(Vector3.forward * Time.deltaTime * speed);

		//rotate to direction
		transform.Rotate(Vector3.up * Time.deltaTime * turningRate * direction);
	}
/*

shrimp = GameObject.Find("model_C2011_SHRIMP_RedBee");
Log.Assert(shrimp!=null, "Missing shrimp configuration in CharacterManager.cs!");
shrimp.renderer.material.shader = Shader.Find("Takomat/Timeline"); (?)
shrimp.renderer.material.color.a

*/

/* phenome style of setting character values

	for ( int i = 0; i < size; i++)
	{
		app::entity* e = app::trinigy_entitymanager::Instance()->entity_by_index( i);
		std::string name = e->get_entity_key();
		Managers::BehaviorManager::Instance()->SendPose( 
		                                                name.c_str(), 	//character name
		                                                "smile", 		//pose id in behavior.xml 
		                                                1000.0f,		// duration in s
		                                                1.0f, 		//target weight
		                                                10.6f);		//fadein in s
	} 

Model_ShrimGlow_Healthy 
- Model_ShrimGlow_Dead
- Model_ShrimGlow_Sick

*/
	///<summary>
	/// This function will executed itself preidically
	/// and will change the current speed and direction
	/// based on random number.
	///</summary>
	private void ExecuteAI()
	{
		//get random number

		if(!gameObject.active)
		{
			//only update if running
			return;
		}

		//generate command randomly, 
		//the first 10 is used, high number wont make any effect
		int i = Random.Range(0, 20);
		float nextTime = aiSpeed;

		//execute one of the command based on the value
		switch(i)
		{
			case 0:
				//stop for a bit
				speed = 0;
				direction = 0;
				nextTime += 2;
				model.Health = AquaSphere.MODEL.HealthType.HEALTHY;
				break;

			case 1:
				//accelerate
				speed += acceleration;
				nextTime += 3;
				model.Health = AquaSphere.MODEL.HealthType.SICK;
				break;

			case 2:
				//slow
				speed -= acceleration;
				nextTime += 2;
				model.Health = AquaSphere.MODEL.HealthType.HEALTHY;
				break;

			case 3:
				//right
				direction += rotation;

				break;

			case 4:
				//left
				direction -= rotation;

				break;

			case 5:
				//face straight
				direction = 0;

				break;

			//
			case 6:
				//run!!
				speed = maxSpeed;
				nextTime +=3;

				break;

			case 7:
				//spin a bit
				direction += rotation * 1.3f;
				nextTime = 1;

				break;

			case 8:
				//back and turn
				direction = rotation * 1.2f;
				speed = minSpeed;
				nextTime = 2;

				break;

			case 9:
				//back and turn
				direction = rotation * -1.2f;
				speed = minSpeed;
				nextTime = 2;

				break;

			case 10:
				//rest a bit
				direction = 0;
				speed = 0;
				nextTime = 5;

				break;

			default:
				//nothing
				break;
		}
		Invoke("ExecuteAI", nextTime);

	}

	/// Process dialog events and trigger the views of the effect like bubbles
	/// </summary>
	public void ProcessCharacterEvent(string eventId, string param)
	{
		 //	GameManager.Instance.Event("CHARACTER", "REDBEE_HEALTH", "SICK");
	}
}
}
