//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Epigene;
using Epigene.GAME;
using Epigene.MODEL;
using Epigene.UI;
using Epigene.VIEW;
using Epigene.AUDIO;

using AquaSphereMini.MODEL.GAME;

//TODO: add enter, update, exit

//------------------------------------------------------------------------------
using Epigene.IO;
/*
public static class EditorConfiguration
{
    public static string root = "AquaSphereGameMini";
}
*/

namespace AquaSphereMini
{
    /// <summary>
    /// Start the game. Add each screens
    /// to the UIManager and set the first one to active.
    /// Each screen have to manage their own logic.
    /// This class is only a start point for the game
    /// and does only the basic initialization.
    /// Use this for logics which is not specific for any screen
    /// like log levels or language for the whole game.
    /// You can always alter from these in every screen
    ///
    /// The Layer semantic in AquaSphereMini :
    /// starting with "User Layer 8 HUD"
    ///
    /// HUD        : All elements of head up display
    
    /// </summary>
    public class AquaSphereGameMini : MainGame
    {
        public string buildTime;
        
      
        private DBModuleManager dbModuleManager;
        
        /// Tests the script condition.
        /// </summary>
        /// <param name="value">value to evaulate for condition</param>
        /// <returns>same as value</returns>
        public bool TestScriptConditionEvaulateBool(bool value)
        {
            Log.GameTimes ("TestScriptConditionEvaulateBool");
            
            return value;
        }//TestScriptConditionEvaulateBool()
        
        
//------------------------------------------------------------------------------
        
        // Use this for initialization
        public void Awake()
        {
            Debug.Log ("AquaSphereMini awake.");
            WebPlayerDebugManager.addOutput ("Version build: " + buildTime, 1);
            dbModuleManager = DBModuleManager.Instance;
            if(useDBModule)
                StartCoroutine("addServiceURL");
            
            base.Awake();
            
            //------- TODO WORKAROUND -------
            GameScenario gs = GameScenario.Instance;
            gs.currentDate = System.DateTime.Now;
            
            //Edit StartPoint
            System.DateTime startDate = GameScenario.Instance.currentDate;
            //WebPlayerDebugManager.addOutput(
            // "Get Current Date: " + startDate, 3);
            startDate = startDate.AddDays (-28);


            Log.Info("Starting AquaSphereMini...");
            
            //create conditions
            ACondition falseCond = new FalseCondition();
            ACondition trueCond  = new TrueCondition();
            ScriptCondition<bool> scriptCond =
                new ScriptCondition<bool>(
                    TestScriptConditionEvaulateBool, true);
        }
        
        public void Start()
        {
            base.Start();
        }


        private IEnumerator addServiceURL()
        {
            yield return new WaitForSeconds(0.1f);
            dbModuleManager.Event(
                                  "ADD_SERVICEURL",
                                  "development",
                                  "takomat-bb-dev.stage.endertech.net");
            dbModuleManager.Event(
                                  "ADD_SERVICEURL",
                                  "staging",
                                  "takomat-bb.stage.endertech.net");
            dbModuleManager.Event(
                                  "ADD_SERVICEURL",
                                  "production",
                                  "www.buergerbeteiligungsspiel.de");
            dbModuleManager.Event(
                                  "ADD_SERVICEURL",
                                  "production_twist",
                                  "www.twist-water.com");
            
            yield return new WaitForSeconds(0.1f);
        }

        public override void Enter()
        {
            Log.Info("ENTER "+gameObject.name);
            AudioManager.Instance.MuteMusic = true;
            
            
            //TODO: add startScreenName
            //ui.AddScreenFromFile(configResources+"/Settings");
            //switch to start screen
            ui.InitUI(gameObject);
            ui.UIParent.transform.parent = gameObject.transform;
            ui.ActivateScreen(startScreenName);
            
            GameManager.Instance.RegisterEventHandler(
                "CONFIG", ProcessConfigEvent, gameObject);
        }
        
        public override void Exit()
        {
            Log.Info("EXIT "+gameObject.name);
            GameManager.Instance.RemoveEventHandler(
                "CONFIG", ProcessConfigEvent);
        }
        
        // public void CreateScreen(string name)
        // {
        // 	string script = configResources+"/"+name+".txt";
        
        // }
        
        public void ProcessConfigEvent(string eventId, string data)
        {
            if (eventId == "ScenarioLoad")
            {   // the web download
                GameScenario gameScenario = GameScenario.Instance;
                gameScenario.Load ();
                gameScenario.DownloadStartScenario();
            }
            else if (eventId == "StartScenarioLoad")
            {   // standalone procedure
                GameScenario gameScenario = GameScenario.Instance;
                gameScenario.LoadStartScenario(data);
            }
            else if (eventId == "StartScenarioLoaded")
            {   // everything has been loaded, so start the SIM
                gm.Start ("SIM");
            }
        }//EventHandler()
        
    }//class StartGame
    
}//namespace
//------------------------------------------------------------------------------