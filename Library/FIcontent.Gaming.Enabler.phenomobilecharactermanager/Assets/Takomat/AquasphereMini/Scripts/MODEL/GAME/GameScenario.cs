//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;
using Epigene.MODEL;
using Epigene.IO;
using Epigene;
//------------------------------------------------------------------------------
using System.Collections.Generic;
using Epigene.GAME;
using AquaSphere.MODEL;

namespace AquaSphereMini.MODEL.GAME
{
/// <summary>
/// This class handles the game scenarios saved 
/// in a non volatile storage, e.g. xml, mySQL database ...
/// It is the interface to the different storages
/// put the data into the MODELs
/// !!!DBModule must be checked in the MainGame to be called!!!
/// </summary>
public sealed class GameScenario
{
    GameConfiguration 		gameConfiguration 	= GameConfiguration.Instance;

    
    // we use double here, because in epigene 
    // Flash version(ActionSctipt 3)  we use Number 
    // which is a data type representing 
    // an IEEE-754 double-precision floating-point number 
    // that might change to int, we will see.
    public int 		currentYear				= 2014;   	// 2014 A
    public int 		currentMonth			= 5;  		// start 5		
    public int 		currentDay		 		= 1;    	// start 1
    public DateTime currentDate 			= new DateTime (1970, 1, 1);

    public double 	minutesPerYear			= 1.0;		//minutes/year SIM
    public double 	minutesPerWeek			= 2.5;		//minutes/week CP
    public double 	P0006_balance			= 101.0;	//million Euro
    public double 	simulationSpeed			= 1.0;
    public double 	timeStep                = 1.0; // in second
    public double 	maxTime                =  34.0; // in second

    public double 	cpDurationInDays		= 42;		//days

    public int 		simStartYear			= 0;   	// 2010 A 
    public int 		simStartMonth			= 1;  		// start 1		
    public int 		simStartDay		 		= 1;    	// start 1
    public DateTime simStartDate 			= new DateTime (1970, 1, 1);
    public double 	simDurationInYears		= 16;		//years

    public int		numberOfHouses			= 80;		//houses

//------------------------------------------------------------------------------
    //Action
    private static readonly GameScenario instance = new GameScenario();
                 
    /// <summary>
    /// Gets the instance.
    /// </summary>
    /// <value>The instance.</value>
    public static GameScenario Instance
    {
        get{ return instance;}
    }

    // Use this for initialization
    //void Start () 
    private GameScenario()
    {
        currentDate 	 = DateTime.Now;
        currentYear  	 = currentDate.Year;
        currentMonth	 = currentDate.Month;
        currentDay	 	 = currentDate.Day;
    }//GameScenario()

//------------------------------------------------------------------------------
    // Use this for initialization
    //void Start () 
    public void DownloadStartScenario ()
    {
        WebPlayerDebugManager.addOutput (
            "HALLO LARS  DownloadStartScenario  : ", 4);


        Debug.Log("Load Start Scenario...");
        WebPlayerDebugManager.addOutput("Load Start Scenario...",1);

        #if UNITY_IOS
            TextAsset download =
            Resources.Load("ExternalFiles/assets/startScenario") as TextAsset;

            LoadStartScenario (download.text);
            return;
        #endif

        #if UNITY_EDITOR
        // For requested JSON with Editor
        DBModuleManager.Instance.Event
            ("START_SCENARIO_LOAD",
            Application.dataPath,
            "/Resources/ExternalFiles");
        #elif UNITY_STANDALONE
        // For requested JSON with Standalone
        if (GameManager.Instance.standaloneBetaMode)
        {
            DBModuleManager.Instance.Event
                ("START_SCENARIO_LOAD",
                Application.dataPath,
                "");
        }
        else
        {
            TextAsset download =
            Resources.Load("ExternalFiles/assets/startScenario") as TextAsset;
            LoadStartScenario (download.text);
        }

        #elif UNITY_WEBPLAYER
        // For requested JSON with web player
        WebPlayerDebugManager.addOutput (
            "Application Data Path: " + Application.dataPath, 1);
        
        DBModuleManager.Instance.Event
            ("START_SCENARIO_LOAD",
            null,
            "/epigene_unity3d");
        #endif
    }
    
    /// <summary>
    /// Event handlers
    /// </summary>
    public void SetDateScenario(Dictionary<string,object> obj)
    {
		double simDurationInWeeks = 0.0;
        if (obj.ContainsKey("startYear"))
            simStartYear = int.Parse(obj["startYear"].ToString());
        else
            simStartYear = currentYear;
        
        if (obj.ContainsKey("runTime"))
            simDurationInYears = int.Parse(obj["runTime"].ToString());
		if (obj.ContainsKey("runTimeWeeks"))
			simDurationInWeeks = int.Parse(obj["runTimeWeeks"].ToString());
			if (obj.ContainsKey("totalRealTime"))
		{
			double totalRealTime = double.Parse(obj["totalRealTime"].ToString()); // in minutes
			maxTime = totalRealTime*60.0f; // in seconds
			timeStep = maxTime / (simDurationInWeeks*7); // time in seconds per day
		}
        //if (obj.ContainsKey("maxTime"))
        //    maxTime = double.Parse(obj["maxTime"].ToString());
    }
    
    public void LoadStartScenario (string startScenarioText)
    {
        /*
        WebPlayerDebugManager.addOutput ("HALLO LARS    : ", 4);
         */
        Dictionary<string,object> startScenario =
            MiniJSON.Json.Deserialize(startScenarioText)
                as Dictionary<string,object>;

        foreach(Dictionary<string,object> dict
                in ((List<object>)startScenario["startScenario"]))
        {
            foreach(Dictionary<string,object> obj
                    in ((List<object>)dict["objects"]))
            {
                if(obj.ContainsKey("id"))
                {

                }
            }
        }
        WebPlayerDebugManager.addOutput("startScenario.txt loaded. Mini", 0);
        GameManager.Instance.Event ("CONFIG", "StartScenarioLoaded", "");
    }

    public void Load()
    {
        Log.Info("<color=cyan>GameScenario Load</color> initialized.");

        // start to load config scenario data 

        //TODO Check wether or not there's saved data and if so, use the savedData DateTime
        currentDate = DateTime.Now;
        currentYear  	 = currentDate.Year;
        currentMonth	 = currentDate.Month;
        currentDay	 	 = currentDate.Day;


        if (gameConfiguration.HasKey ("minutesPerYear"))
            minutesPerYear				=
            double.Parse (gameConfiguration.GetValue("minutesPerYear"));
        if (gameConfiguration.HasKey ("minutesPerWeek"))
            minutesPerWeek				=
            double.Parse (gameConfiguration.GetValue("minutesPerWeek"));
        if (gameConfiguration.HasKey ("P0006_balance"))
            P0006_balance				= double.Parse (gameConfiguration.GetValue("P0006_balance"));
        if (gameConfiguration.HasKey ("simulationSpeed"))
            simulationSpeed				= double.Parse (gameConfiguration.GetValue("simulationSpeed"));


        if (gameConfiguration.HasKey ("cpDurationInDays"))
            cpDurationInDays			= double.Parse (gameConfiguration.GetValue("cpDurationInDays"));


        //WebPlayerDebugManager.addOutput("People change: " + numberOfPeople, 4);



    }

}//GameScenario
}//namespace