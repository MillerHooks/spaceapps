//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;
using Epigene;
using System.IO;
using Epigene.GAME;
//------------------------------------------------------------------------------
using Epigene.IO;

namespace AquaSphere.IO
{
	
	public sealed class BuildLibrary 
	{
		public bool	  initialized	= false;		
		//public Dictionary<string, Dictionary<string,  Dictionary<string, string>>> library;
		public Dictionary<string, Dictionary<string,  List<string>>> library;
		
//------------------------------------------------------------------------------
		
		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <value>The instance.</value>
		public static BuildLibrary Instance
		{
			get{ return instance;}
		}
		static readonly BuildLibrary instance = new BuildLibrary();
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Epigene.IO.AppConfiguration"/> class.
		/// </summary>
		BuildLibrary()
		{			
			GameManager.Instance.RegisterEventHandler("CONFIG", ProcessConfigEvent);
		}//AppConfiguration()
		
		public void Load(string dataPath)
		{
			WebPlayerDebugManager.addOutput("Start loading Build Library..", 1);

			string dataPathStandalone = dataPath.Remove(dataPath.LastIndexOf("."));
			string fullPath = "";

			#if UNITY_EDITOR
			// For requested XML with Editor
			fullPath = "file://" + Application.dataPath + "/Resources/ExternalFiles/" + dataPath;
			#elif UNITY_STANDALONE
			// For requested JSON with Standalone
			fullPath = "file://" + Application.dataPath + "/"+ dataPath;
			
			/*
			 * if (GameManager.Instance.standaloneBetaMode)
			{
				fullPath = "file://" + Application.dataPath + dataPath;
			}

			else
			{
				TextAsset download = Resources.Load("ExternalFiles"+dataPathStandalone) as TextAsset;
				
				Initialize (download.text);
				return;
			}*/
			#elif UNITY_WEBPLAYER
			// For requested JSON with web player
			fullPath = Application.dataPath + dataPath;
			#elif UNITY_IOS
			TextAsset download = Resources.Load("ExternalFiles"+dataPathStandalone) as TextAsset;
			
			Initialize (download.text);
			return;
			#endif
			DBModuleManager.Instance.Event(
				"CONFIG",
				fullPath,
				"BuildLibrary");
		}
		
		public void ProcessConfigEvent(string eventId, string data)
		{
			if (eventId == "BuildLibrary")
				Initialize(data);
		}
		
		public void Initialize(string dataString)
		{	
			Dictionary<string,object> dict = 
				MiniJSON.Json.Deserialize(dataString) as Dictionary<string,object>;

			WebPlayerDebugManager.addOutput(
				"Start Initializing Library Data, size: " + dataString.Length.ToString() , 1);

			library = new Dictionary<string, Dictionary<string, List<string>>>();

			foreach(Dictionary<string,object> menus
			        in ((List<object>)dict["BuildLibrary"]))
			{
				foreach(Dictionary<string,object> menu
				        in ((List<object>)menus["menus"]))
				{
					Dictionary<string, List<string>> menuDict = new Dictionary<string, List<string>>();

					foreach(Dictionary<string,object> submenu
					        in ((List<object>)menu["submenus"]))
					{								
						List<string> submenuDict = new List<string>();

						foreach(Dictionary<string,object> obj
						        in ((List<object>)submenu["objects"]))
						{	
							if(obj.ContainsKey("id"))
							{
								submenuDict.Add(obj["id"].ToString());
									/*
								WebPlayerDebugManager.addOutput("\n" +
									menu["id"].ToString() + "\n  " + 
									submenu["id"].ToString() + "\n    " +
									obj["id"].ToString(), 1);
								/**/
							}
						}	
						menuDict.Add(
							submenu["id"].ToString(),
							submenuDict);
					}
					library.Add(
						menu["id"].ToString(),
						menuDict);
				}
			}

			initialized = true;

			GameManager.Instance.Event(
				"INIT",
				"Library",
				"");
		}
	}// IPAppConfiguration
	
}// AquaSphere.IO
//------------------------------------------------------------------------------
