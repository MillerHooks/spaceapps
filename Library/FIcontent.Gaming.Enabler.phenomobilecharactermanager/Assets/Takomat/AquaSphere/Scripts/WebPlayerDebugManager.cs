//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Epigene;
using Epigene.GAME;

//------------------------------------------------------------------------------
//namespace AquaSphere



namespace AquaSphere
{
	public class WebPlayerDebugManager : Epigene.WebPlayerDebugManager
	{		


		private int numberOfShrimps = 1;
		//F0000_Finance finance;

		void buildEnvironment()
		{
			GameManager.Instance.Event(
				"NANOCUBE",
				"addSoil",
				"M0200_SOIL_Deponitmix");

			GameManager.Instance.Event(
				"NANOCUBE",
				"addSoil",
				"M0300_SOIL_Shrimpgravel");

			GameManager.Instance.Event(
				"NANOCUBE",
				"addWater",
				"M0410_WATER");
		}

		void BuyShrimp()
		{
			if (numberOfShrimps == 0) return;
			for (int i = 0; i < numberOfShrimps; i++)
			{
				GameManager.Instance.Event(
					"NANOCUBE",
					"addShrimp",
					"C2010_SHRIMP_RedBee");
			}
		}

		void OnGUI() {

			if (Epigene.GAME.GameManager.Instance.debugMode == false)
				return;

				gameObject.SetActive (true);
			// Make the toggle button.
			if (GUI.Button (new Rect (20, 15, 80, 20), labelShowHide)) {
				if (showDebug == false) {
					showDebug = true;
					labelShowHide = "hide";
				} else {
					showDebug = false;
					labelShowHide = "show";
				}
			}

			if (showDebug == true) {
				// Make a background box
				GUI.Box (new Rect (10, 10, 320, 250), "");

				// Make the clear button.
				if (GUI.Button (new Rect (105, 15, 80, 20), "clear")) {
					longString = "";
					debugOutput.Clear();
					//addOutput ("Version build: " + RetrieveLinkerTimestamp ().ToString (), 0);
				}
				
				// Make the Info only button.
				if (GUI.Button (new Rect (245, 15, 80, 20), "All")) {
					allButton();
				}
				
				// Make the Info only button.
				if (GUI.Button (new Rect (245, 40, 80, 20), "Info")) {
					infoButton();
				}
				
				// Make the GameTimes only button.
				if (GUI.Button (new Rect (245, 65, 80, 20), "GameTimes")) {
					gametimesButton();
				}
				
				// Make the GameTimes only button.
				if (GUI.Button (new Rect (245, 90, 80, 20), "Warning")) {
					warningButton();
				}
				
				// Make the GameTimes only button.
				if (GUI.Button (new Rect (245, 115, 80, 20), "Error")) {
					errorButton();
				}
				
				// Make the GameTimes only button.
				if (GUI.Button (new Rect (245, 140, 80, 20), "Exception")) {
					exceptionButton();
				}

				// Make the Build Environment button.
				if (GUI.Button (new Rect (30, 200, 105, 20), "Build Start")) {
					buildEnvironment();
				}


				// Make the Buy Shrimp button.
				numberOfShrimps = int.Parse(GUI.TextField (new Rect (30, 230, 105, 20), numberOfShrimps.ToString()));

				if (GUI.Button (new Rect (140, 230, 100, 20), "Buy Shrimp")) {
					BuyShrimp();
				}
				/*
				// Make the GoToEndScreen button.
				if (GUI.Button (new Rect (95, 200, 70, 20), title)) {
					if (title == "Win")
					{
						toEndScreen("GameSuccess");
						title = "Lose";
					}
					else if (title == "Lose")
					{
						toEndScreen("GameOver");
						title = "Win";
					}
				}

				// Make the SendHighscore button.
				if (GUI.Button (new Rect (95, 230, 70, 20), "Highscore")) {
					sendHighscore(highscore);
				}

*/
				scrollPosition = GUI.BeginScrollView (
					new Rect (20, 40, 215, 150), 
					scrollPosition, new Rect (10, 20, 190, 1550));
				GUI.Box (new Rect (5, 15, 205, 1550), "");

				if (updateGUI == true) {
					switch (activeButton) {
						case "All":
							allButton();
							break;
						case "Info":
							infoButton();
							break;
						case "GameTimes":
							gametimesButton();
							break;
						case "Warning":
							warningButton();
							break;
						case "Error":
							errorButton();
							break;
						case "Exception":
							exceptionButton();
							break;
						default:
							break;
					}
					updateGUI = false;
				}
				GUI.Label (new Rect (10, 20, 205, 1550), longString);

				GUI.EndScrollView ();
			}
		}
	}
}
