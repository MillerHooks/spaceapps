﻿//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using Epigene.GAME;
using AquaSphere;

public class C1000_PLANTS : MonoBehaviour 
{
	public GameObject Seedling;
	public GameObject Normal;
	public GameObject Rank;
	public Material   Healthy;
	public Material   Sick;
	public Material   Dead;

	public enum Condition {healthy, sick, dead};
	public Condition condition;
	public enum State {seedling, normal, rank};
	public State state;

	public bool GhostMode
	{
		get {return ghostMode;}
		set
		{
			ghostMode = value;
			StopCoroutine("TestGrowOlder");
			/*
			if (value)
			{
				Color transparent = new Color (1, 1, 1, 0.3f);
				Seedling.renderer.material.shader =
                Shader.Find("Transparent/Diffuse");
				Seedling.renderer.material.color = transparent;
				Normal.renderer.material.shader =
                Shader.Find("Transparent/Diffuse");
				Normal.renderer.material.color = transparent;
				Rank.renderer.material.shader =
                Shader.Find("Transparent/Diffuse");
				Rank.renderer.material.color = transparent;
			}/**/
		}
	}
	public bool ghostMode  = false;

	/// <summary>
	/// Enable the object will register event handler
	/// </summary>
	public void OnEnable()
	{
		SetState(State.seedling, Condition.healthy);
		StartCoroutine("TestGrowOlder");
	}

	public void Start()
	{
		gameObject.transform.localPosition = new Vector3(0,0,0);
	}

	public IEnumerator TestGrowOlder()
	{
		float time = 10.0f;
		yield return new WaitForSeconds(time);
		SetState(State.normal, Condition.healthy);
		yield return new WaitForSeconds(time);
		SetState(State.rank, Condition.healthy);
		yield return new WaitForSeconds(time);
		SetState(State.rank, Condition.sick);
		yield return new WaitForSeconds(time);
		SetState(State.rank, Condition.dead);
		yield return new WaitForSeconds(time);
		SetState(State.normal, Condition.dead);
		yield return new WaitForSeconds(time);
		SetState(State.seedling, Condition.dead);
	}

	/// <summary>
	/// Disable the object will remove event handler
	/// </summary>
	public void OnDisable()
	{

	}

	/// <summary>
	/// Function to check new events.
	/// </summary>
	public void EventHandler(string eventId, string param)
	{

	}//EventHandler()

	public void Update()
	{

	}

	public void SetState (State newState, Condition newCondition)
	{
		HideAll();
		GameObject obj = null;
		switch (newState)
		{
		case State.seedling:
			Seedling.SetActive(true);
			obj = Seedling;
			break;
		case State.normal:
			Normal.SetActive(true);
			obj = Normal;
			break;
		case State.rank:
			Rank.SetActive(true);
			obj = Rank;
			break;
		}

		switch (newCondition)
		{
		case Condition.healthy:
			obj.GetComponent<MeshRenderer>().material = Healthy;
			break;
		case Condition.sick:
			obj.GetComponent<MeshRenderer>().material = Sick;
			break;
		case Condition.dead:
			obj.GetComponent<MeshRenderer>().material = Dead;
			break;
		}

		if (! ghostMode) return;
		Color oldColor = obj.GetComponent<MeshRenderer>().material.color;
		Color newColor = new Color(oldColor.r, oldColor.b, oldColor.g, 0.3f);          
		obj.GetComponent<Renderer>().material.SetColor("_Color", newColor); 
		obj.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
	}

	public void HideAll()
	{
		Seedling.SetActive(false);
		Normal.SetActive(false);
		Rank.SetActive(false);
	}
}
