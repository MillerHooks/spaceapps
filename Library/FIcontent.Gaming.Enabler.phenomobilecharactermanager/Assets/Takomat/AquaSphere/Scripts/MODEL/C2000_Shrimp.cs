//------------------------------------------------------------------------------
// Copyright (c) 2014-2015 takomat GmbH and/or its licensors.
// All Rights Reserved.

// The coded instructions, statements, computer programs, and/or related material
// (collectively the "Data") in these files contain unpublished information
// proprietary to takomat GmbH and/or its licensors, which is protected by
// German federal copyright law and by international treaties.

// The Data may not be disclosed or distributed to third parties, in whole or in
// part, without the prior written consent of takoamt GmbH ("takomat").

// THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
// ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. TAKOMAT MAKES NO
// WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
// BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE,
// NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE.
// WITHOUT LIMITING THE FOREGOING, TAKOMAT DOES NOT WARRANT THAT THE OPERATION
// OF THE DATA WILL gameengine_dialogsmanagerBE UNINTERRUPTED OR ERROR FREE.

// IN NO EVENT SHALL TAKOMAT, ITS AFFILIATES, LICENSORS BE LIABLE FOR ANY LOSSES,
// DAMAGES OR EXPENSES OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR
// MULTIPLE DAMAGES OR OTHER SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL,
// LOSS OF PROFITS, REVENUE OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES
// OR DAMAGES OF ANY KIND), HOWEVER CAUSED, AND REGARDLESS
// OF THE THEORY OF LIABILITY, WHETHER DERIVED FROM CONTRACT, TORT
// (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
// ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
// WHETHER OR NOT TAKOMAT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
// OR DAMAGE.
//------------------------------------------------------------------------------
// This class is part of the epigene(TM) Software Framework.
// All license issues, as above described, have to be negotiated with the
// takomat GmbH, Cologne.
//------------------------------------------------------------------------------
using UnityEngine;
using System;
using Epigene;
using Epigene.UI;


//------------------------------------------------------------------------------
namespace AquaSphere.MODEL
{
	public enum HealthType {DEAD, SICK, HEALTHY, AAAVITALITY};

	/// <summary>
	/// C2000_ shrimp is the base class for shrimps data model.
	/// </summary>
	public class C2000_Shrimp : Resource
	{
        public C2000_Shrimp()
        {
        Debug.Log("<color=cyan>C2010_CrystalRed</color> initialized.");
        }

		public string Name
		{
			get {return name;}
			set {name = value; }
		}		
		private string name	= "";

        public float CurrentSize
        {
            get {return currentSize;}
            set {currentSize = value; }
        }
        private float currentSize	= 0.0f;

        public float ThresholdOxygenDeadlyLowMax;
        // mg/l concentration in the water
        public float thresholdOxygenDeadlyLowMax
        {
            get { return thresholdOxygenDeadlyLowMax;  }
            set { thresholdOxygenDeadlyLowMax = value; }
        }

        public float thresholdOxygenOptimal;
        // mg/l concentration in the water
        public float ThresholdOxygenOptimal
        {
            get { return thresholdOxygenOptimal;  }
            set { thresholdOxygenOptimal = value; }
        }

        public float thresholdOxygenDeadlyHighMax;
        // mg/l concentration in the water
        public float ThresholdOxygenDeadlyHighMax
        {
            get { return thresholdOxygenDeadlyHighMax;  }
            set { thresholdOxygenDeadlyHighMax = value; }
        }


		public HealthType health;
		// set is dead, healthy, sick, AAAVitality
		public HealthType Health
		{
			get { return health;  }
			set { health = value; }
		}

/*
"sex": "",
"pregnant": ""  ,
"pregnancyCurrent": "0"  ,
"pregnancyDuration": "28"  ,
"averageOffspringMin": "30"  ,
"averageOffspringMax": "50"  ,
"health": "0"  ,
"ageBiological": "0"  ,
"ageCurrent": "0"  ,
"ageMaturity": "90"  ,
"ageLifespanMin": "365"  ,
"ageLifespanMax": "730"  ,
"ageStatus": "0"  ,
"sizeAdultMin": "2"  ,
"sizeAdultMax": "3"  ,
"sizeBirth": "0,2"  ,
"sizeGrowth": "0,3"  ,
"currentSize": "0"  ,

"thresholdOxygenOptimal": "8"  ,
"thresholdOxygenDeadlyLowMin": "4"  ,
"thresholdOxygenDeadlyLowMax": "2"  ,
"DaysOxygenDeadlyLow": "7"  ,
"thresholdOxygenDeadlyHighMin": "12"  ,
"thresholdOxygenDeadlyHighMax": "16"  ,
"DaysOxygenDeadlyHigh": "7"  ,

"thresholdFoodOptimal": "7"  ,
"thresholdFoodDeadlyLowMin": "4"  ,
"thresholdFoodDeadlyLowMax": "1"  ,
"DaysFoodDeadlyLow": "14"  ,
"thresholdFoodDeadlyHighMin": "10"  ,
"thresholdFoodDeadlyHighMax": "14"  ,
"DaysFoodDeadlyHigh": "14"  ,
"food": "7"  ,
"oxygen": "8"  ,
"ammonium/ammonia": "0"  ,
"ammonium/ammonia": "0.05"  ,
"thresholdAmmoniaOptimal": "0"  ,
"thresholdAmmoniaDeadlyHighMin": "0.02"  ,
"thresholdAmmoniaDeadlyHighMax": "0.2"  ,
"ammonmia intake plants": "0,1"  ,
"carbondioxid": "3"  ,
"carbondioxid": "0"  ,
"thresholdCarbondioxidOptimal": "0.5"  ,
"thresholdCarbondioxidDeadlyLowMin": "x"  ,
"thresholdCarbondioxidDeadlyLowMax": "x"  ,
"daysCarbondioxidDeadlyLow": "x"  ,
"thresholdCarbondioxidDeadlyHighMin": "30"  ,
"thresholdCarbondioxidDeadlyHighMax": "50"  ,
"daysCarbondioxidDeadlyHigh": "14"  ,
"weightAdult": "0.1"  ,
"weightLarva": "0.5"  ,
we have to take teh miminum threshold values
*/

    }

}//namespace
//------------------------------------------------------------------------------
