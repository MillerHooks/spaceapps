// Shader created with Shader Forge v1.06 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.06;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:2,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:True,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:False,dith:0,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.8235294,fgcg:0.1211073,fgcb:0.1211073,fgca:1,fgde:0.04,fgrn:0.45,fgrf:2,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:8218,x:33244,y:32400,varname:node_8218,prsc:2|diff-4256-OUT,alpha-2996-A;n:type:ShaderForge.SFN_Lerp,id:8973,x:32779,y:32220,varname:node_8973,prsc:2|A-8910-RGB,B-5761-RGB,T-2996-RGB;n:type:ShaderForge.SFN_Color,id:8910,x:32530,y:32116,ptovrint:False,ptlb:node_8910,ptin:_node_8910,varname:node_8910,prsc:2,glob:False,c1:0,c2:0.04313726,c3:0.145098,c4:1;n:type:ShaderForge.SFN_Color,id:5761,x:32520,y:32300,ptovrint:False,ptlb:node_8910_copy,ptin:_node_8910_copy,varname:_node_8910_copy,prsc:2,glob:False,c1:0.01362458,c2:0.4632353,c3:0.05393461,c4:1;n:type:ShaderForge.SFN_Tex2d,id:2996,x:32520,y:32511,ptovrint:False,ptlb:node_2996,ptin:_node_2996,varname:node_2996,prsc:2,tex:271f5ee3273dd7f4fae6e204d4f8c4bf,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4256,x:33018,y:32315,varname:node_4256,prsc:2|A-8973-OUT,B-2996-R;proporder:8910-5761-2996;pass:END;sub:END;*/

Shader "Shader Forge/verlauf" {
    Properties {
        _node_8910 ("node_8910", Color) = (0,0.04313726,0.145098,1)
        _node_8910_copy ("node_8910_copy", Color) = (0.01362458,0.4632353,0.05393461,1)
        _node_2996 ("node_2996", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _node_8910;
            uniform float4 _node_8910_copy;
            uniform sampler2D _node_2996; uniform float4 _node_2996_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 indirectDiffuse = float3(0,0,0);
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb*2; // Ambient Light
                float4 _node_2996_var = tex2D(_node_2996,TRANSFORM_TEX(i.uv0, _node_2996));
                float3 diffuse = (directDiffuse + indirectDiffuse) * (lerp(_node_8910.rgb,_node_8910_copy.rgb,_node_2996_var.rgb)*_node_2996_var.r);
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor,_node_2996_var.a);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _node_8910;
            uniform float4 _node_8910_copy;
            uniform sampler2D _node_2996; uniform float4 _node_2996_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i)*2;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _node_2996_var = tex2D(_node_2996,TRANSFORM_TEX(i.uv0, _node_2996));
                float3 diffuse = directDiffuse * (lerp(_node_8910.rgb,_node_8910_copy.rgb,_node_2996_var.rgb)*_node_2996_var.r);
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * _node_2996_var.a,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
