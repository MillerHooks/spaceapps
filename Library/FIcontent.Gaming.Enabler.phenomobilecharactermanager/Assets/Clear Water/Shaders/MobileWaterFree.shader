Shader "Rutger/MobileWaterFree" {
    Properties {
        _TintColor ("Tint Color", Color) = (1,1,1,1)
        _WaterSpeed ("Water Speed", Float ) = 1
        _Water ("Water", 2D) = "white" {}
        _WaterDistanceFade ("Water Distance Fade", Float ) = 20
        _WaterDistance ("Water Distance", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 2.0
            uniform float4 _TimeEditor;
            uniform float _WaterSpeed;
            uniform sampler2D _WaterDistance; uniform float4 _WaterDistance_ST;
            uniform float4 _TintColor;
            uniform float _WaterDistanceFade;
            uniform sampler2D _Water; uniform float4 _Water_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float3 emissive = _TintColor.rgb;
                float3 finalColor = emissive;
                float4 node_96 = _Time + _TimeEditor;
                float node_192 = (node_96.r*_WaterSpeed);
                float2 node_995 = i.uv0;
                float2 node_1176 = (node_995.rg+node_192*float2(1,0));
                float2 node_1355 = ((node_995.rg+node_192*float2(0,1))+float2(0.12345,0.1234));
                float node_1517 = lerp((tex2D(_Water,TRANSFORM_TEX(node_1176, _Water)).r*tex2D(_Water,TRANSFORM_TEX(node_1355, _Water)).r),(tex2D(_WaterDistance,TRANSFORM_TEX(node_1176, _WaterDistance)).r*tex2D(_WaterDistance,TRANSFORM_TEX(node_1355, _WaterDistance)).r),saturate(pow((distance(i.posWorld.rgb,_WorldSpaceCameraPos.rgb)/_WaterDistanceFade),2.0)));
                float node_1524 = (i.vertexColor.a*node_1517);
/// Final Color:
                return fixed4(finalColor,(node_1524+node_1517));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"   
}
