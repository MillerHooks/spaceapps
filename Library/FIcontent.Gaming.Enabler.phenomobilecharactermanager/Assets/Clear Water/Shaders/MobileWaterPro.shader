Shader "Rutger/MobileWaterPro" {
    Properties {
        _TintColor ("Tint Color", Color) = (1,1,1,1)
        _WaterSpeed ("Water Speed", Float ) = 1
        _Water ("Water", 2D) = "white" {}
        _WaterDistanceFade ("Water Distance Fade", Float ) = 20
        _WaterDistance ("Water Distance", 2D) = "white" {}
        _Fresnel ("Fresnel", Range(0.1, 5)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 2.0
            uniform sampler2D _GrabTexture;
            uniform float4 _TimeEditor;
            uniform float _WaterSpeed;
            uniform sampler2D _WaterDistance; uniform float4 _WaterDistance_ST;
            uniform float4 _TintColor;
            uniform float _WaterDistanceFade;
            uniform sampler2D _Water; uniform float4 _Water_ST;
            uniform float _Fresnel;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.screenPos = o.pos;
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
	/////// Normals:
                float3 normalDirection =  i.normalDir;
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float4 node_96 = _Time + _TimeEditor;
                float node_192 = (node_96.r*_WaterSpeed);
                float2 node_995 = i.uv0;
                float2 node_1176 = (node_995.rg+node_192*float2(1,0));
                float2 node_1355 = ((node_995.rg+node_192*float2(0,1))+float2(0.12345,0.1234));
                float2 node_1498 = (i.vertexColor.a*lerp((tex2D(_Water,TRANSFORM_TEX(node_1176, _Water)).rgb*tex2D(_Water,TRANSFORM_TEX(node_1355, _Water)).rgb),(tex2D(_WaterDistance,TRANSFORM_TEX(node_1176, _WaterDistance)).rgb*tex2D(_WaterDistance,TRANSFORM_TEX(node_1355, _WaterDistance)).rgb),saturate(pow((distance(i.posWorld.rgb,_WorldSpaceCameraPos.rgb)/_WaterDistanceFade),2.0)))).rg;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (node_1498*float2(0,0.1));
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
	////// Lighting:
	////// Emissive:
                float node_1531 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel);
                float3 emissive = (_TintColor.rgb+node_1531);
                float3 finalColor = emissive;
	/// Final Color:
                return fixed4(lerp(sceneColor.rgb, finalColor,node_1498.r),1);
            }
            ENDCG
        }
    }
    FallBack "Rutger/MobileWaterFree"    
}
