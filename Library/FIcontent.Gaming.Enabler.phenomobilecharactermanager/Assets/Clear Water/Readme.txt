*Thank you for purchasing my "Clear Water" mobile shader package!*
--------------------------------------------------------------
Clear Water is a Unity water shader created for mobile platforms. 
This water shader is lightweight and optimized to run on low-end devices using shader target 2.0.
The package comes with both a free and a pro version. Depending on which Unity version you're using.

Enjoy!
--------------------------------------------------------------
Requirements for MobileWaterPro:
Unity 4.x Pro
Shader Model 2.0 or up.

Requirements for MobileWaterFree:
Unity 4.x 
Shader Model 2.0 or up.

*The differences between the two shaders is the refraction effect. This effect is only available in Unity Pro.

*Shader settings:*
--------------------------------------------------------------
Tint Color: (_TintColor)
Gives your water textures a tint color.
--------------------------------------------------------------
Water speed: (_WaterSpeed)
The scrolling speed of the water.
--------------------------------------------------------------
Water: (_Water)
The main water texture.
--------------------------------------------------------------
Water Distance fade: (_WaterDistanceFade)
The distance at which the second water texture is used for different waves.
--------------------------------------------------------------
Water distance: (_WaterDistance)
The second water texture, used for different waves.
--------------------------------------------------------------
Fresnel: (_Fresnel)
Fresnel/rimlight which creates a surface brightness effect.
--------------------------------------------------------------

*Tips:*
--------------------------------------------------------------
- Paint the ground below the water surface the color which the water should be. 
(For example: create a texture with some deep water colors or use vertex colors to achieve this effect.)

- Give the edges of your water surface "plane" a vertex color alpha. This will give a soft edge/transition of the intersecting geometry.

- Using less tiling of the main water texture for when you're close to the water surface. For the distance use a more "wavy/brighter" texture.


*Demo scene:*
--------------------------------------------------------------
The example scene contains a pool model/environment which is uses tiling textures + vertex colors for the baked lighting effect.
--------------------------------------------------------------

Rutger Stegenga
URL: www.rutgerstegenga.nl
EMAIL: info@rutgerstegenga.nl
