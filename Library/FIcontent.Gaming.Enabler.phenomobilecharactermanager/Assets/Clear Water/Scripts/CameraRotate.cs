﻿using UnityEngine;
using System.Collections;

public class CameraRotate : MonoBehaviour {
	public Transform target;
	public float RotationSpeed = 100f;
	void Update () {
		transform.RotateAround(target.position, Vector3.up,RotationSpeed*Time.deltaTime);
	}
}