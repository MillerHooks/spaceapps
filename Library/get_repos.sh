#!/bin/bash
filename='repo_list'
echo Start
while read p; do 
    git clone $p
done < $filename
find . -name .git -exec rm -rf {} \;
find . -name .gitignore -exec rm -rf {} \;
