# About #
Team SimMars3003 is roughly proposing an interactive game using
Mars data from NASA with Unity3D as the front end and PostGIS/nginx/python as a 
back end as needed. 

This game might look something like a sim city on mars or also might be something
else by the end of the day.

Any AR or VR libraries come from the EU's http://mediafi.org/
Creative Commons Licensed marker detection, SLAM, 3d audio mixing, 
GIS integration, and some visual assets.
# Requirements #
Docker Tools - https://www.docker.com/products/docker-toolbox
Blender - https://www.blender.org/
Unity3d - https://unity3d.com/
Pure Data - https://puredata.info/downloads/pd-extended

# Includes #
No necissarily used wholesale, but advanced and licensed publically.

https://github.com/fi-content2-games-platform
    